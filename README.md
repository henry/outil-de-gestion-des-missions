# Outil de gestion des missions
Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL  
florence.henry@obspm.fr  
https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions

Ensemble de squelettes SPIP, CSS et JS pour gérer les demandes de missions et produire les bilans, notamment pour labos1.5

## Configuration de SPIP

### Identification / Authentification

Il est nécessaire que les agents soient identifiés lorsqu'ils déposent une demande de mission. Le plus simple est de configurer le serveur Apache en ajoutant une authentification de type Basic basée sur le LDAP.

Dans SPIP, il faut aussi activer le LDAP. Ainsi, SPIP peut faire le lien entre l'utilisateur authentifié par Apache et le compte SPIP.

Quand un utilisateur se connecte pour la 1e fois et que LDAP est activé, SPIP va aller chercher dans l'annuaire LDAP le nom et le mail de la personne et initialiser le compte SPIP.

### Mots-clés

Dans Configuration > Contenu du site > Les mots-clés :

* Utiliser les mots-clés
* Utiliser la configuration avancée des groupes de mots-clés

Dans Édition > Mots-clés :

* Créer un nouveau groupe de mots "Applications"
* Le restreindre aux seuls Articles
* Cocher "On ne peut sélectionner qu’un seul mot-clé à la fois dans ce groupe."
* Y mettre les mots-clés 
	* Missions
	* Gestion des demandes de mission

### Fonctions

Ajouter cette fonction dans `mes_fonctions.php` :

```
function my_strftime($timestamp, $format) {
  setlocale(LC_ALL, 'fr_FR');
	return strftime($format, $timestamp);
}
```


### Squelettes

Dans les squelettes, ajouter le code suivant dans le `<header>` de toutes les pages :

```
<BOUCLE_appli(MOTS) {id_article=#ENV{id_article}} {type=Applications}>

    #SET{charger_mission_js,0}
    [(#TITRE|=={Missions}|oui) #SET{charger_mission_js,1}]
    [(#TITRE|=={Gestion des demandes de mission}|oui) #SET{charger_mission_js,1}]

    [(#GET{charger_mission_js}|=={1}|oui)
        [<script src="(#CHEMIN{prive/javascript/ui/jquery-ui.js}|url_absolue)" type="text/javascript"></script>]
        [<script src="(#CHEMIN{prive/javascript/ui/jquery.ui.autocomplete.js}|url_absolue)" type="text/javascript"></script>]
        [<script src="(#CHEMIN{javascript/geonames.js}|url_absolue)" type="text/javascript"></script>]
        [<script src="(#CHEMIN{javascript/entites.js}|url_absolue)" type="text/javascript"></script>]
        [<script src="(#CHEMIN{javascript/missions.js}|url_absolue)" type="text/javascript"></script>]


        [<link rel="stylesheet" type="text/css" media="screen,projection,print" href="(#CHEMIN{css/jquery-ui.css}|url_absolue)" />]
        [<link rel="stylesheet" type="text/css" media="screen,projection,print" href="(#CHEMIN{css/missions.css}|url_absolue)" />]
    ]

    [(#TITRE=={Bilan carbone}|oui)
      [<script type="text/javascript" src="(#CHEMIN{lib/highcharts/v7.2.1/highcharts.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/highcharts/v7.2.1/modules/data.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/highcharts/v7.2.1/modules/exporting.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/highcharts/v7.2.1/modules/export-data.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/highcharts/v7.2.1/modules/annotations.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{javascript/highcharts-options.js})"></script>]
    ]

    [(#TITRE=={Bilan carbone - carte}|oui)
      [<script type="text/javascript" src="(#CHEMIN{lib/highmaps/v7.2.1/code/highmaps.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/highcharts/v7.2.1/modules/exporting.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/highcharts/v7.2.1/modules/export-data.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/highmaps/v7.2.1/mapdata/world.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{lib/proj4js/v2.3.6/proj4.js})"></script>]
      [<script type="text/javascript" src="(#CHEMIN{javascript/highcharts-options.js})"></script>]
    ]

	</BOUCLE_appli>

``` 

Ajouter le code suivant dans le `<body>` de toutes les pages de type article. Attention à ce que ce code soit mis à l'intérieur d'une boucle ARTICLES:

```
[
(#INCLURE{fond=inc/applications}{id_article}{env}|trim) 
]

```


## Plugins SPIP nécessaires

* Accès restreint
* CVT Upload
* Formidable
* Facteur

### Configuration du plugin Accès restreint
Plusieurs zones sont nécessaires :

* Gestionnaires
* Administration
* Commission environnement

il faut les créer avec ce nom précis dans l'interface de gestion des zones d'accès restreint et y mettre les personnes faisant partie des gestionnaires du labo, ou de la direction.

L'ajout de répertoire dans ces zones est optionnel. Les zones servent essentiellement à définir des droits.


### Configuration du plugin Facteur
Configuration > Facteur
* Définir les nom et adresse mail 
* Méthode d’envoi : Utiliser SMTP
* Hôte : smtp.obspm.fr
* Port : 25
* Authentification : non
* Connexion sécurisée : TLS

le reste peut rester tel quel.

Effectuer le test de la configuration.

### Configuration du plugin Formidable
Configuration > Gestion des plugins 
Dans la liste, cliquer sur l'icône avec les outils dans l'encart de Formidable (url `/ecrire/?exec=configurer_formidable`)

Lier les formulaires aux contenus : Articles; Auteurs
Cocher "Utiliser les auteurs pour les formulaires"


## Gestion des entités dépensières

L'outil nécessite de connaître les entités dépensières du laboratoire, avec les scientifiques responsables de chaque entité et les gestionnaires en charge (afin d'envoyer les emails aux bonnes personnes).

Il faut créer un formulaire qui permet de gérer ces entités. Dans la page Édition > Formulaires, importer le formulaire suivant :

`formulaires/yaml/formulaire-gestion_entites_depensieres.yaml`

La liste déroulante permettant le choix des responsables scientifiques est celle des auteurs dans spip. Il faut donc que chaque personne ait un compte actif dans SPIP. 

La liste des gestionnaire doit par contre être définie à la main (il ne m'a pas été possible de relier formidable et accès restreint). Il faut donc aller dans formidable > Gestion des entités dépensières > Configurer les champs et éditer la configuration du champ "Gestionnaire" puis sélectionner dans la liste des auteurs les gestionnaires.

Le champ "Crédits" peut être modifier au besoin en ajoutant ou en retirant des boutons radio.

### Utilisation du formulaire de gestion des entités

Idéalement dans un répertoire accessible uniquement aux Gestionnaires, créer un article "Nouvelle entité dépensière" avec le code

```
<formulaire|formidable|id=gestion_entites_depensieres>

[Retour à la liste des entités->rub250]
```

Le lien vers la page contenant la liste des entités est à adapter selon votre site `rub110` (pour la rubrique 110) ou `art360` (pour l'article 360) par exemple.

Vous pouvez afficher n'importe où dans le site le tableau des entités avec le code
```
<entites|identifiant=gestion_entites_depensieres|modifier=oui>
ou
<entites|identifiant=gestion_entites_depensieres|modifier=non>
```

l'argument `modifier` permet de dire si on souhaite permettre la modification de l'entité.

Au LESIA, on a une rubrique "Gestion des entités dépensières" protégée dans la zone des gestionnaires contenant l'article avec `modifier=oui`. Ailleurs dans le site, on a un article accessible à tous avec `modifier=non`.

Une fois saisies, ces entités seront utilisées dans le formulaire de gestion des missions. Quand une entité sera sélectionnée, le ou la gestionnaire sera automatiquement sélectionné(e), ainsi que le responsable scientifique. Cela économise une saisie pour l'utilisateur et évite les erreurs.

## Gestion des missions

### Création du formulaire

Dans la page Édition > Formulaires , importer le formulaire suivant :

`formulaires/yaml/formulaire-demande_mission.yaml`

Comme pour le formulaire précédent, le champ "Gestionnaire" est à personnaliser avec le nom des gestionnaires du labo.

### Utilisation du formulaire

Dans un espace accessible à tous, créer un article "Nouvelle demande de mission" avec pour contenu
```
<nouvelle_demande_mission0>
```
Ce sera la page de demande de mission. Repérer le numéro de l'article.
Y ajouter le mot-cle "Missions". 


### Gestion des missions côté agent

Créer un article "Mes demandes de mission"  avec pour contenu :
```
<acces|id=4215|recalcul=4215>
<demandes_mission0>
```
Remplacer 4215 par le numéro de l'article "Nouvelle demande de mission". Y ajouter le mot-cle "Missions". 

### Gestion des missions côté gestionnaires

Dans une zone réservée aux Gestionnaires, créer un article "Gestion des demandes de mission" sans contenu. Y ajouter le mot-clé "Gestion des demandes de mission".

Aucun contenu n'est nécessaire. Toute la logique est dans le mot-clé "Gestion des demandes de mission".

### Description du processus

1. Un agent remplit le formulaire de demande de mission
2. Un accusé de réception lui est envoyé avec le détail de sa mission
3. Un mail est également envoyé au gestionnaire de l'entité
4. Le gestionnaire doit alors :
	* aller sur le lien du mail pour visualiser la demande
	* [optionnel] cliquer sur "Modifier la mission" puis "Envoyer une demande de confirmation au responsable" si nécessaire
	* [le cas échéant] cliquer sur "Annuler la mission" et renseigner le motif si nécessaire
	* saisir le numéro d'ordre de mission et cliquer sur "Mettre à jour l'OM".
	Cette saisie permet de faire passer la mission du statut "En attente" à "En cours".
5. La mission passe dans les missions passées quand la date de retour est échue.

## Effectuer le bilan carbone

Le bilan effectue des moyennes, des médianes, des histogrammes... Le calcul est lourd, il n'est à faire qu'une à 2 fois par an.
Depuis la base de données, il faut exécuter le script
```
sql/missions.sql
```

Il peut y avoir des erreurs dans les saisies des agents. Il ne faut pas hésiter à rectifier les données manifestement fausses. On ne s'en rend parfois compte qu'en affichant le bilan des missions. J'ai eu par exemple le cas d'une mission dont les étapes n'étaient pas saisies dans le bon ordre.

Exemple de rectifications :

```
UPDATE spip_formulaires_reponses_champs SET valeur=368 
   WHERE id_formulaires_reponse=1737 and nom='input_59'; -- dist_1
UPDATE spip_formulaires_reponses_champs SET valeur=5.9 
   WHERE id_formulaires_reponse=1737 and nom='input_46'; -- emission_co2_1
UPDATE spip_formulaires_reponses_champs SET valeur=11.8 
   WHERE id_formulaires_reponse=1737 and nom='input_56'; -- total_emission_co2

UPDATE spip_formulaires_reponses_champs SET valeur='2019-11-24 16:00:00' 
   WHERE id_formulaires_reponse=2252 and nom='date_4'; -- date_depart_2
UPDATE spip_formulaires_reponses_champs SET valeur='2019-12-04 09:00:00' 
   WHERE id_formulaires_reponse=2229 and nom='date_4'; -- date_depart_2

UPDATE spip_formulaires_reponses_champs SET valeur='MGMIS' WHERE id_formulaires_reponse in 
	(1711, 1740, 1757, 1775, 1777, 1778, 1779) and nom='input_12'; -- entite
	
UPDATE spip_formulaires_reponses_champs SET valeur=1156 
   WHERE id_formulaires_reponse in (2130, 2131) and nom='auteurs_1'; -- id_demandeur
UPDATE spip_formulaires_reponses_champs SET valeur=55 
   WHERE id_formulaires_reponse=2154 and nom='auteurs_1'; -- id_demandeur


UPDATE spip_formulaires_reponses_champs set valeur='2019-11-24 10:00:00' 
   WHERE id_formulaires_reponse=2218 and nom='date_4' ; -- date_depart_2,
UPDATE spip_formulaires_reponses_champs set valeur='Sydney, Australie' 
   WHERE id_formulaires_reponse=2218 and nom='input_13' ; -- ville_depart_2,
UPDATE spip_formulaires_reponses_champs set valeur='-33.86785' 
   WHERE id_formulaires_reponse=2218 and nom='hidden_5' ; -- lat_ville_depart_2,
UPDATE spip_formulaires_reponses_champs set valeur='151.20732' 
   WHERE id_formulaires_reponse=2218 and nom='hidden_6' ; -- lon_ville_depart_2,

```

Après update des réponses, il faut refaire tourner `sql/missions.sql`.


### Bilan carbone du laboratoire pour une année
Créer un article doté du mot-clé "Bilan carbone" et avec le contenu :

```
<bilancarbone2019>
```

Pour chaque année, il faut faire un nouvel article avec le numéro de l'année passée en paramètre du modèle.

### Carte des destinations
Créer un article doté du mot-clé "Bilan carbone - carte" et avec le contenu :

```
<bilancarbone2019|carte>
```

Cette page affiche :

* la distribution des lieux de mission par type de transport
* la distribution des émissions par continent

Les membres de la zone "Commission environnement" verront un bouton "Mettre à jour les continents". C'est une opération qui prend plusieurs minutes et
qui peut envoyer plusieurs milliers de requête à Geonames. Geonames plafonne le nombre de requête à 10000/jour.
Il faut l'exécuter après avoir fait tourner le script `missions.sql`. Si on dépasse le quota, il suffit de recommencer le lendemain (le script ne traitera que les destinations non traitées la veille).

TODO : envoyer un mail en fin de traitement pour savoir si on atteint le quota ou si toutes les destinations ont été identifiées.


### Bilan carbone d'un agent
Seul un agent peut voir son propre bilan. Personne d'autre ne peut y accéder.

Créer un article doté du mot-clé "Bilan carbone" et avec le contenu :

```
<bilancarbone|agent|annee=2019>
```

Cela affichera son bilan carbone ainsi que celui des entités dépensières dont il est responsable.

### Préparer le bilan des missions pour Labos1point5 

Exécuter dans la base de données :
```
sql/bilan1.5-2019.sql
```

Le site labos1point5 attend des noms de pays en Français et j'obtiens de temps en temps le nom du pays en Anglais (sans doute à cause de navigateurs en anglais côté utilisateurs). Il peut donc être nécessaire de rectifier certaines saisies.

## Fichiers nécessaires

```
balise/force_auth.php
css/missions.css
formulaires/annuler_mission.html
formulaires/annuler_mission.php
formulaires/demander_confirmation_mission.html
formulaires/demander_confirmation_mission.php
formulaires/maj_continent_mission.html
formulaires/maj_continent_mission.php
formulaires/maj_om_mission.html
formulaires/maj_om_mission.php
formulaires/yaml/formulaire-demande_mission.yaml
formulaires/yaml/formulaire-deplacements_domicile_travail.yaml
formulaires/yaml/formulaire-gestion_entites_depensieres.yaml
inc/applications.html
inc/gestion_missions/afficher.html
inc/gestion_missions/annuler.html
inc/gestion_missions/appli.html
inc/gestion_missions/gestion_mission.js
inc/gestion_missions/missions_annulees.html
inc/gestion_missions/missions_autresgestionnaires.html
inc/gestion_missions/missions_en_cours.html
inc/gestion_missions/missions_gestionnaire.html
inc/gestion_missions/missions_passees.html
inc/gestion_missions/modifier.html
inc/gestion_missions/nouvelle_mission.html
inc/gestion_missions/table.html
inc/missions/appli.html
javascript/entites.js
javascript/geonames.js
javascript/highcharts-options.js
javascript/missions.js
modeles/acces.html
modeles/bilancarbone.html
modeles/bilancarbone_agent.html
modeles/bilancarbone_agent_evolution.html
modeles/bilancarbone_carte.html
modeles/bilancarbone_cumul_agent_co2.html
modeles/bilancarbone_cumul_agent_distance.html
modeles/bilancarbone_cumul_co2_distance.html
modeles/bilancarbone_cumul_co2_nb.html
modeles/bilancarbone_dist_agent_co2.html
modeles/bilancarbone_dist_agent_distance.html
modeles/bilancarbone_dist_agent_nb_missions.html
modeles/bilancarbone_dist_co2_route.html
modeles/bilancarbone_dist_co2_train.html
modeles/bilancarbone_dist_continent.html
modeles/bilancarbone_dist_entite_co2.html
modeles/bilancarbone_dist_entite_distance.html
modeles/bilancarbone_dist_mission.html
modeles/bilancarbone_dist_mission_temps.html
modeles/bilancarbone_dist_trajet.html
modeles/bilancarbone_dist_trajet_transport.html
modeles/bilancarbone_entite.html
modeles/bilancarbone_entites.html
modeles/demandes_mission.html
modeles/entites.html
modeles/entites_liste.html
modeles/nouvelle_demande_mission.html
notifications/annuler_mission.html
notifications/demande_confirmation_mission.html
notifications/formulaire_demande_mission_accuse.html
notifications/formulaire_demande_mission_email.html
sql/bilan1.5-2019.sql
sql/missions-bilan.sql
sql/missions-fct.sql
sql/missions.sql

```

### Rectifications 

1. Le fichier JS `javascript/geonames.js` contient mon token pour interroger les webservices de Geonames et OpenRouteService. 

	Il est nécessaire de vous créer un compte pour les 2 services (c'est gratuit) et d'y mettre vos propres tokens.

	```
	var geoname_user = 'votre_user';
	var openrouteservice_token = "votre_token";
	```

	* https://openrouteservice.org/dev/#/signup
	* https://www.geonames.org/login

2. Le squelette `modeles/nouvelle_demande_mission.html` contient du code qui interroge notre base de données afin de pré-remplir le champ "statut". 

	```
	#SET{statut, ''}
	[(#SET{email, #SESSION{email}|strtolower})]
	<BOUCLE_pers(personnels:agent){lower(email)=#GET{email}}>
	   [(#IDSTATUT|in_any{#ARRAY{1,1,6,6,7,7}}|oui) #SET{statut, 'ita'}]
	   [(#IDSTATUT|in_any{#ARRAY{2,2,3,3,4,4,8,8}}|oui) #SET{statut, 'chercheur'}]
	   [(#IDSTATUT|in_any{#ARRAY{5,5,9,9}}|oui) #SET{statut, 'doc-post-doc'}]
	</BOUCLE_pers>
	
	```
	
	Vous devrez supprimer ce code (qui fera planter vos squelettes car la base de données `personnels` ne sera pas déclarée comme base secondaire pour spip. Vous pouvez le remplacer par un code similaire qui récupère cette donnée par un autre moyen. Vous pouvez par exemple avoir un fichier CSV contenant cette information et utiliser une boucle DATA.
	
3. Le squelette `inc/missions/appli.html` contient principalement le codes qui va initialiser la liste des entités dépensière. Mais il contient aussi la recherche des congés des gestionnaires, afin d'afficher le cas échéant si le/la gestionnaire est ou sera en congés dans les 8 prochains jours :

	```
	<select id="conges_gestionnaires" style="display: none;">
	<BOUCLE_gestionnaires(spip_zones_liens spip_zones){titre=Gestionnaires}{objet=auteur}>
	<BOUCLE_gestionnaire(AUTEURS){id_auteur=#ID_OBJET}>
	<BOUCLE_agent(personnels:agent){email like #EMAIL*}>
	<BOUCLE_conges(personnels:conge){idagent=#IDAGENT}{par debut}{debut <= #GET{dans8jours}}{fin >= #GET{aujourdhui}}>
		<option class="#_gestionnaire:ID_AUTEUR" data-debut-conge="[(#DEBUT|affdate)]" data-fin-conge="[(#FIN|affdate)]" >
	</BOUCLE_conges>
	</BOUCLE_agent>
	</BOUCLE_gestionnaire>
	</BOUCLE_gestionnaires>
	</select>
	```
	
	Comme pour le code précédent, vous pouvez juste supprimer ce code, en ne laissant que `<select id="conges_gestionnaires" style="display: none;"></select>`, ou l'adapter pour interroger votre propre base de données.
	
	Connaître les congés du gestionnaire dont on dépend permet de choisir un autre gestionnaire dans la liste si la demande de mission est urgente.

## Mises à jour

Depuis que le code a été distribué l'an dernier, quelques ajouts ont été faits dans le formulaire. Je ne connais pas de méthode simple pour mettre à jour les champs. Voilà les champs à ajouter ou à modifier :

* Bloc "Missionnaire"
	* [ajout] `oui_non_3` : Le demandeur est-il le missionnaire ?
		* Valeur oui : "on"
		* Valeur non : "non"
		* Champ obligatoire
	* [ajout] `selection_22` : Statut
		* Choix possibles :
		
			```
			chercheur|Chercheur.e-EC
			ita|ITA
			doc-post-doc|Doc, Post-doc
			invite|Personne invitée
			```
		* Ce champ pourra être rempli automatiquement si possible
		* "invité" vaut pour toute personne invitée par le laboratoire (même si on connait le statut chercheur/ita de la personne).
		* champ demandé pour l'export vers labos1point5
* Bloc "Description de la mission"
	* [ajout] `radio_3` : Motif
		* Choix possibles :

			```
			etude|Observations, expérimentations
			colloque|Colloque-Congrès
			seminaire|Séminaire
			enseignement|Enseignement
			collaboration|Collaboration
			visite|Visite
			adm|Administration de la recherche
			autre|Autre
			```
		* Champ demandé pour l'export vers labos1point5. Le premier choix est proposé sous le label "Etudes de terrain" par labos1point5. Nous l'avons adapaté par ce qui nous semble être notre terrain en astro : les observations, l'instrumentation.
		* Champ obligatoire
	* [modif] `textarea_1` renommé en "Description" au lieu de "Motif détaillé.
* Bloc "Itinéraire pendant la mission"
	* [modif] `explication_15` : mise en exergue de la phrase :
			```
			[(Ne pas renseigner l'étape domicile - gare/aéroport)]
			```
	* [modif] `input_9` : 
		* Placeholer : Ne pas renseigner l’étape domicile - gare/aéroport
		
Attention à ce que les champs aient bien ces noms (radio_3, oui_non_3...). Sinon, penser à adapter le code JS dans `javascript/geonames.js`.

Pour le champ "statut", voilà un exemple de code SQL à exécuter pour ajouter ce champ aux anciennes réponses (3403 est le numéro de la première réponse avec le champ statut renseigné et 15 est le numéro du formulaire; ces valeurs sont à adapter) :

```
-- Florence Henry est ITA
INSERT INTO spip_formulaires_reponses_champs (id_formulaires_reponse, nom, valeur, maj)
   SELECT DISTINCT id_formulaires_reponse, 'selection_22', 'ita', now() 
   FROM spip_formulaires_reponses_champs
   WHERE id_formulaires_reponse IN (
      SELECT id_formulaires_reponse 
      FROM spip_formulaires_reponses_champs
      WHERE nom='input_45' and replace(lower(valeur), 'observatoiredeparis.psl.eu', 'obspm.fr')  = 'florence.henry@obspm.fr') 
   AND id_formulaires_reponse IN (
      SELECT id_formulaires_reponse 
      FROM spip_formulaires_reponses 
      WHERE id_formulaire = 15)
   AND id_formulaires_reponse < 3403; 
   
-- Ceux qui n'ont pas d'adresse mail en obspm.fr ou psl.eu sont des invités
INSERT INTO spip_formulaires_reponses_champs (id_formulaires_reponse, nom, valeur, maj)
   SELECT DISTINCT id_formulaires_reponse, 'selection_22', 'invite', now() 
   FROM spip_formulaires_reponses_champs
   WHERE id_formulaires_reponse IN (
      SELECT id_formulaires_reponse 
      FROM spip_formulaires_reponses_champs
      WHERE nom='input_45' 
      AND replace(lower(valeur), 'observatoiredeparis.psl.eu', 'obspm.fr')  not like '%@obspm.fr%' )
   AND id_formulaires_reponse IN (
      SELECT id_formulaires_reponse 
      FROM spip_formulaires_reponses 
      WHERE id_formulaire = 15)
   AND id_formulaires_reponse < 3403; 
   
```
