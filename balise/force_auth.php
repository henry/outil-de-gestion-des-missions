<?php

/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */

if (!defined("_ECRIRE_INC_VERSION")) return; 

function balise_FORCE_AUTH($p) {
        $args = array();

        $p->descr['session'] = TRUE;

        return calculer_balise_dynamique($p, 'FORCE_AUTH', $args);
}


function balise_FORCE_AUTH_stat($args, $filters) {
        $static = "";
        return array($static);
}

function balise_FORCE_AUTH_dyn($static) {
        if (isset($GLOBALS['auteur_session']['id_auteur']))
                return;

        $auth = charger_fonction('auth', 'inc');
        $save = $GLOBALS['meta']["ldap_statut_import"];

        $res = $auth();
        verifier_session(true);
        
        $GLOBALS['meta']["ldap_statut_import"] = $save;
}


