--
-- Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
-- florence.henry@obspm.fr
-- https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
--
-- This file is part of "OGM - Outil de gestion des missions".
-- 
-- OGM is free software: you can redistribute it and/or modify
-- it under the terms of the Affero GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- OGM is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with OGM.
--
 

select 
'# mission', 'Date de départ', 'Ville de départ', 'Pays de départ', 
'Ville de destination', 'Pays de destination', 'Mode de déplacement', 
'Nb de personnes dans la voiture', 'Aller Retour (OUI si identiques, NON si différents)', 
'Motif du déplacement (optionnel)', 'Statut de l\'agent (optionnel)'
union all
select 
id_mission, date_format(date_depart, '%d/%m/%Y') as date_depart, 
trim(left(ville_depart, char_length(ville_depart)-instr(reverse(ville_depart), ','))) as ville_depart,
trim(substr(ville_depart, char_length(ville_depart)-instr(reverse(ville_depart), ',')+2)) as pays_depart, 
trim(left(ville_arrivee, char_length(ville_arrivee)-instr(reverse(ville_arrivee), ','))) as ville_arrivee,
trim(substr(ville_arrivee, char_length(ville_arrivee)-instr(reverse(ville_arrivee), ',')+2)) as pays_arrivee, 
case mode_transport
 when 'avion'              then 'Avion'   
 when 'car'                then 'Bus'
 when 'taxi'               then 'Taxi'
 when 'vlocation'          then 'Voiture personnelle'
 when 'vpersonnel'         then 'Voiture personnelle'
 when 'tgvfrance'          then 'Train'
 when 'tgvinternational'   then 'Train'
 when 'trainfrance'        then 'Train'
 when 'traininternational' then 'Train'
 when 'tgvinternational'   then 'Train'
end as type_transport,
case mode_transport
 when 'taxi'        then '1'
 when 'vlocation'        then '1'
 when 'vpersonnel'        then '1'
 else ''
end as nb_personnes,
'NON' as aller_retour,
motif as motif,
statut as statut
from trajets_realises  
where year(date_depart)=2019 
and mode_transport != 'vadministratif' and mode_transport != 'covoiturage' 
-- INTO OUTFILE '/tmp/missions2019.tsv' 
-- FIELDS ENCLOSED BY '"' 
-- TERMINATED BY '\t' 
-- ESCAPED BY '"' 
-- LINES TERMINATED BY '\r\n';