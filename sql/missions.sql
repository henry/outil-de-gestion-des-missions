--
-- Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
-- florence.henry@obspm.fr
-- https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
--
-- This file is part of "OGM - Outil de gestion des missions".
-- 
-- OGM is free software: you can redistribute it and/or modify
-- it under the terms of the Affero GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- OGM is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with OGM.
--

source missions-fct.sql;


DROP TABLE IF EXISTS missions_realisees;
select 'creation missions_realisees';
CREATE TABLE missions_realisees as (
   SELECT 
   r.id_formulaires_reponse as id_mission,
   r.date as date,
   r.id_auteur as  id_auteur,
   true as demandeur_missionnaire,
   1 as etape_destination,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_41' and id_formulaires_reponse=r.id_formulaires_reponse) as nb_modification,
   (select valeur from spip_formulaires_reponses_champs where nom='input_6' and id_formulaires_reponse=r.id_formulaires_reponse) as num_ordre_mission,
   (select valeur from spip_formulaires_reponses_champs where nom='oui_non_4' and id_formulaires_reponse=r.id_formulaires_reponse) as visa,
   (select valeur from spip_formulaires_reponses_champs where nom='oui_non_5' and id_formulaires_reponse=r.id_formulaires_reponse) as annulee,
   (select valeur from spip_formulaires_reponses_champs where nom='input_44' and id_formulaires_reponse=r.id_formulaires_reponse) as motif_annulation,
   (select valeur from spip_formulaires_reponses_champs where nom='auteurs_1' and id_formulaires_reponse=r.id_formulaires_reponse) as id_demandeur,
   (select valeur from spip_formulaires_reponses_champs where nom='input_4' and id_formulaires_reponse=r.id_formulaires_reponse) as nom_missionnaire,
   (select valeur from spip_formulaires_reponses_champs where nom='input_45' and id_formulaires_reponse=r.id_formulaires_reponse) as email_missionnaire,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_22' and id_formulaires_reponse=r.id_formulaires_reponse) as statut_missionnaire,
   (select valeur from spip_formulaires_reponses_champs where nom='input_5' and id_formulaires_reponse=r.id_formulaires_reponse) as num_passeport,
   (select valeur from spip_formulaires_reponses_champs where nom='date_1' and id_formulaires_reponse=r.id_formulaires_reponse) as date_passeport,
   (select valeur from spip_formulaires_reponses_champs where nom='date_2' and id_formulaires_reponse=r.id_formulaires_reponse) as date_expiration_passeport,
   (select valeur from spip_formulaires_reponses_champs where nom='radio_1' and id_formulaires_reponse=r.id_formulaires_reponse) as type,
   (select valeur from spip_formulaires_reponses_champs where nom='input_7' and id_formulaires_reponse=r.id_formulaires_reponse) as montant_forfait,
   (select valeur from spip_formulaires_reponses_champs where nom='input_12' and id_formulaires_reponse=r.id_formulaires_reponse) as entite,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_42' and id_formulaires_reponse=r.id_formulaires_reponse) as justificatif,
   (select valeur from spip_formulaires_reponses_champs where nom='destinataires_1' and id_formulaires_reponse=r.id_formulaires_reponse) as id_gestionnaire,
   (select valeur from spip_formulaires_reponses_champs where nom='auteurs_2' and id_formulaires_reponse=r.id_formulaires_reponse) as id_responsable,
   (select valeur from spip_formulaires_reponses_champs where nom='radio_3' and id_formulaires_reponse=r.id_formulaires_reponse) as motif,
   (select valeur from spip_formulaires_reponses_champs where nom='textarea_1' and id_formulaires_reponse=r.id_formulaires_reponse) as description,
   (select valeur from spip_formulaires_reponses_champs where nom='case_1' and id_formulaires_reponse=r.id_formulaires_reponse) as mission_altitude,
   (select valeur from spip_formulaires_reponses_champs where nom='fichiers_1' and id_formulaires_reponse=r.id_formulaires_reponse) as fichier_justificatif,
   (select valeur from spip_formulaires_reponses_champs where nom='input_1' and id_formulaires_reponse=r.id_formulaires_reponse) as institut,
   (select valeur from spip_formulaires_reponses_champs where nom='input_2' and id_formulaires_reponse=r.id_formulaires_reponse) as adresse,
   (select valeur from spip_formulaires_reponses_champs where nom='input_3' and id_formulaires_reponse=r.id_formulaires_reponse) as pays,
   (select valeur from spip_formulaires_reponses_champs where nom='input_8' and id_formulaires_reponse=r.id_formulaires_reponse) as montant_inscription,
   (select valeur from spip_formulaires_reponses_champs where nom='input_83' and id_formulaires_reponse=r.id_formulaires_reponse) as monnaie_inscription,
   (select valeur from spip_formulaires_reponses_champs where nom='oui_non_2' and id_formulaires_reponse=r.id_formulaires_reponse) as carte_achat_inscription,
   (select valeur from spip_formulaires_reponses_champs where nom='oui_non_1' and id_formulaires_reponse=r.id_formulaires_reponse) as reservation_hebergement,
   (select valeur from spip_formulaires_reponses_champs where nom='radio_2' and id_formulaires_reponse=r.id_formulaires_reponse) as reservation_transport,
   (select valeur from spip_formulaires_reponses_champs where nom='oui_non_6' and id_formulaires_reponse=r.id_formulaires_reponse) as sejour_prive,
   (select valeur from spip_formulaires_reponses_champs where nom='checkbox_1' and id_formulaires_reponse=r.id_formulaires_reponse) as sejour_prive_avant_apres,
   (select valeur from spip_formulaires_reponses_champs where nom='date_15' and id_formulaires_reponse=r.id_formulaires_reponse) as sejour_prive_avant_date_debut,
   (select valeur from spip_formulaires_reponses_champs where nom='date_16' and id_formulaires_reponse=r.id_formulaires_reponse) as sejour_prive_avant_date_fin,
   (select valeur from spip_formulaires_reponses_champs where nom='date_17' and id_formulaires_reponse=r.id_formulaires_reponse) as sejour_prive_apres_date_debut,
   (select valeur from spip_formulaires_reponses_champs where nom='date_18' and id_formulaires_reponse=r.id_formulaires_reponse) as sejour_prive_apres_date_fin,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_1' and id_formulaires_reponse=r.id_formulaires_reponse) as nb_etapes,
   (select valeur from spip_formulaires_reponses_champs where nom='case_2' and id_formulaires_reponse=r.id_formulaires_reponse) as retour_identique,
   (select valeur from spip_formulaires_reponses_champs where nom='input_9' and id_formulaires_reponse=r.id_formulaires_reponse)       as ville_depart_1,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_1' and id_formulaires_reponse=r.id_formulaires_reponse)      as lat_ville_depart_1,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_2' and id_formulaires_reponse=r.id_formulaires_reponse)      as lon_ville_depart_1,
   (select valeur from spip_formulaires_reponses_champs where nom='input_10' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_1,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_3' and id_formulaires_reponse=r.id_formulaires_reponse)      as lat_ville_arrivee_1,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_4' and id_formulaires_reponse=r.id_formulaires_reponse)      as lon_ville_arrivee_1,
   (select valeur from spip_formulaires_reponses_champs where nom='date_3' and id_formulaires_reponse=r.id_formulaires_reponse)     as date_depart_1,
   (select valeur from spip_formulaires_reponses_champs where nom='input_40' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_1,
   (select valeur from spip_formulaires_reponses_champs where nom='input_42' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_1,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_2' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_1,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_12' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_1,
   (select valeur from spip_formulaires_reponses_champs where nom='input_11' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_1,
   (select valeur from spip_formulaires_reponses_champs where nom='input_59' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_1,
   (select valeur from spip_formulaires_reponses_champs where nom='input_46' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_1,
   (select valeur from spip_formulaires_reponses_champs where nom='input_13' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_2,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_5' and id_formulaires_reponse=r.id_formulaires_reponse)      as lat_ville_depart_2,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_6' and id_formulaires_reponse=r.id_formulaires_reponse)      as lon_ville_depart_2,
   (select valeur from spip_formulaires_reponses_champs where nom='input_14' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_2,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_7' and id_formulaires_reponse=r.id_formulaires_reponse)      as lat_ville_arrivee_2,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_8' and id_formulaires_reponse=r.id_formulaires_reponse)      as lon_ville_arrivee_2,
   (select valeur from spip_formulaires_reponses_champs where nom='date_4' and id_formulaires_reponse=r.id_formulaires_reponse)     as date_depart_2,
   (select valeur from spip_formulaires_reponses_champs where nom='input_41' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_2,
   (select valeur from spip_formulaires_reponses_champs where nom='input_43' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_2,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_3' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_2,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_13' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_2,
   (select valeur from spip_formulaires_reponses_champs where nom='input_15' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_2,
   (select valeur from spip_formulaires_reponses_champs where nom='input_74' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_2,
   (select valeur from spip_formulaires_reponses_champs where nom='input_47' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_2,
   (select valeur from spip_formulaires_reponses_champs where nom='input_37' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_3,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_9' and id_formulaires_reponse=r.id_formulaires_reponse)      as lat_ville_depart_3,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_10' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_3,
   (select valeur from spip_formulaires_reponses_champs where nom='input_38' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_3,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_11' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_3,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_12' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_3,
   (select valeur from spip_formulaires_reponses_champs where nom='date_14' and id_formulaires_reponse=r.id_formulaires_reponse)       as date_depart_3,
   (select valeur from spip_formulaires_reponses_champs where nom='input_48' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_3,
   (select valeur from spip_formulaires_reponses_champs where nom='input_49' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_3,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_11' and id_formulaires_reponse=r.id_formulaires_reponse)  as mode_transport_3,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_14' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_3,
   (select valeur from spip_formulaires_reponses_champs where nom='input_39' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_3,
   (select valeur from spip_formulaires_reponses_champs where nom='input_75' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_3,
   (select valeur from spip_formulaires_reponses_champs where nom='input_50' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_3,
   (select valeur from spip_formulaires_reponses_champs where nom='input_34' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_4,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_19' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_depart_4,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_27' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_4,
   (select valeur from spip_formulaires_reponses_champs where nom='input_35' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_4,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_33' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_4,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_40' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_4,
   (select valeur from spip_formulaires_reponses_champs where nom='date_13' and id_formulaires_reponse=r.id_formulaires_reponse)       as date_depart_4,
   (select valeur from spip_formulaires_reponses_champs where nom='input_58' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_4,
   (select valeur from spip_formulaires_reponses_champs where nom='input_76' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_4,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_10' and id_formulaires_reponse=r.id_formulaires_reponse)  as mode_transport_4,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_15' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_4,
   (select valeur from spip_formulaires_reponses_champs where nom='input_36' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_4,
   (select valeur from spip_formulaires_reponses_champs where nom='input_66' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_4,
   (select valeur from spip_formulaires_reponses_champs where nom='input_73' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_4,
   (select valeur from spip_formulaires_reponses_champs where nom='input_31' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_5,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_18' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_depart_5,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_26' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_5,
   (select valeur from spip_formulaires_reponses_champs where nom='input_32' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_5,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_32' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_5,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_39' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_5,
   (select valeur from spip_formulaires_reponses_champs where nom='date_12' and id_formulaires_reponse=r.id_formulaires_reponse)       as date_depart_5,
   (select valeur from spip_formulaires_reponses_champs where nom='input_57' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_5,
   (select valeur from spip_formulaires_reponses_champs where nom='input_65' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_5,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_9' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_5,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_16' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_5,
   (select valeur from spip_formulaires_reponses_champs where nom='input_33' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_5,
   (select valeur from spip_formulaires_reponses_champs where nom='input_77' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_5,
   (select valeur from spip_formulaires_reponses_champs where nom='input_72' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_5,
   (select valeur from spip_formulaires_reponses_champs where nom='input_28' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_6,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_17' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_depart_6,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_25' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_6,
   (select valeur from spip_formulaires_reponses_champs where nom='input_29' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_6,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_31' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_6,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_38' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_6,
   (select valeur from spip_formulaires_reponses_champs where nom='date_11' and id_formulaires_reponse=r.id_formulaires_reponse)       as date_depart_6,
   (select valeur from spip_formulaires_reponses_champs where nom='input_55' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_6,
   (select valeur from spip_formulaires_reponses_champs where nom='input_64' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_6,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_8' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_6,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_17' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_6,
   (select valeur from spip_formulaires_reponses_champs where nom='input_30' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_6,
   (select valeur from spip_formulaires_reponses_champs where nom='input_78' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_6,
   (select valeur from spip_formulaires_reponses_champs where nom='input_71' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_6,
   (select valeur from spip_formulaires_reponses_champs where nom='input_25' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_7,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_16' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_depart_7,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_24' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_7,
   (select valeur from spip_formulaires_reponses_champs where nom='input_26' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_7,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_30' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_7,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_37' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_7,
   (select valeur from spip_formulaires_reponses_champs where nom='date_10' and id_formulaires_reponse=r.id_formulaires_reponse)       as date_depart_7,
   (select valeur from spip_formulaires_reponses_champs where nom='input_54' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_7,
   (select valeur from spip_formulaires_reponses_champs where nom='input_63' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_7,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_7' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_7,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_18' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_7,
   (select valeur from spip_formulaires_reponses_champs where nom='input_27' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_7,
   (select valeur from spip_formulaires_reponses_champs where nom='input_79' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_7,
   (select valeur from spip_formulaires_reponses_champs where nom='input_70' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_7,
   (select valeur from spip_formulaires_reponses_champs where nom='input_22' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_8,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_15' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_depart_8,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_23' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_8,
   (select valeur from spip_formulaires_reponses_champs where nom='input_23' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_8,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_29' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_8,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_36' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_8,
   (select valeur from spip_formulaires_reponses_champs where nom='date_9' and id_formulaires_reponse=r.id_formulaires_reponse)     as date_depart_8,
   (select valeur from spip_formulaires_reponses_champs where nom='input_53' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_8,
   (select valeur from spip_formulaires_reponses_champs where nom='input_62' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_8,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_6' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_8,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_19' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_8,
   (select valeur from spip_formulaires_reponses_champs where nom='input_24' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_8,
   (select valeur from spip_formulaires_reponses_champs where nom='input_80' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_8,
   (select valeur from spip_formulaires_reponses_champs where nom='input_69' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_8,
   (select valeur from spip_formulaires_reponses_champs where nom='input_19' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_9,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_14' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_depart_9,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_22' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_9,
   (select valeur from spip_formulaires_reponses_champs where nom='input_20' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_9,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_28' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_9,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_35' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_9,
   (select valeur from spip_formulaires_reponses_champs where nom='date_8' and id_formulaires_reponse=r.id_formulaires_reponse)     as date_depart_9,
   (select valeur from spip_formulaires_reponses_champs where nom='input_52' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_9,
   (select valeur from spip_formulaires_reponses_champs where nom='input_61' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_9,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_5' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_9,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_20' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_9,
   (select valeur from spip_formulaires_reponses_champs where nom='input_21' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_9,
   (select valeur from spip_formulaires_reponses_champs where nom='input_81' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_9,
   (select valeur from spip_formulaires_reponses_champs where nom='input_68' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_9,
   (select valeur from spip_formulaires_reponses_champs where nom='input_16' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_depart_10,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_13' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_depart_10,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_21' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_depart_10,
   (select valeur from spip_formulaires_reponses_champs where nom='input_17' and id_formulaires_reponse=r.id_formulaires_reponse)      as ville_arrivee_10,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_20' and id_formulaires_reponse=r.id_formulaires_reponse)  as lat_ville_arrivee_10,
   (select valeur from spip_formulaires_reponses_champs where nom='hidden_34' and id_formulaires_reponse=r.id_formulaires_reponse)  as lon_ville_arrivee_10,
   (select valeur from spip_formulaires_reponses_champs where nom='date_7' and id_formulaires_reponse=r.id_formulaires_reponse)     as date_depart_10,
   (select valeur from spip_formulaires_reponses_champs where nom='input_51' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_air_10,
   (select valeur from spip_formulaires_reponses_champs where nom='input_60' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_route_10,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_4' and id_formulaires_reponse=r.id_formulaires_reponse)   as mode_transport_10,
   (select valeur from spip_formulaires_reponses_champs where nom='selection_21' and id_formulaires_reponse=r.id_formulaires_reponse)  as vehicule_adm_10,
   (select valeur from spip_formulaires_reponses_champs where nom='input_18' and id_formulaires_reponse=r.id_formulaires_reponse)      as immatriculation_10,
   (select valeur from spip_formulaires_reponses_champs where nom='input_82' and id_formulaires_reponse=r.id_formulaires_reponse)      as dist_10,
   (select valeur from spip_formulaires_reponses_champs where nom='input_67' and id_formulaires_reponse=r.id_formulaires_reponse)      as emission_co2_10,
   (select valeur from spip_formulaires_reponses_champs where nom='date_5' and id_formulaires_reponse=r.id_formulaires_reponse)     as date_debut_mission,
   (select valeur from spip_formulaires_reponses_champs where nom='date_6' and id_formulaires_reponse=r.id_formulaires_reponse)     as date_fin_mission,
   (select valeur from spip_formulaires_reponses_champs where nom='input_56' and id_formulaires_reponse=r.id_formulaires_reponse)      as total_emission_co2
   from spip_formulaires_reponses r
   where r.id_formulaire=15 
   and r.id_formulaires_reponse in 
      (select id_formulaires_reponse from spip_formulaires_reponses_champs where nom='date_3' and valeur >= '2019-07-01' )
   and r.id_formulaires_reponse in 
      (select id_formulaires_reponse from spip_formulaires_reponses_champs where nom='oui_non_5' and valeur='' )
   and r.id_formulaires_reponse in 
      (select id_formulaires_reponse from spip_formulaires_reponses_champs where nom='radio_2' and valeur!='sans' )
);


-- on calcule la colonne demandeur_missionnaire
UPDATE missions_realisees, 
   (select id_mission from missions_realisees
   where id_mission in 
      (select
            id_mission
         from missions_realisees r
         left outer join spip_auteurs a
         on r.id_demandeur = a.id_auteur
         where
            not (r.nom_missionnaire = a.nom)))
   as missions_autre_voyageur
set missions_realisees.demandeur_missionnaire = false 
where missions_realisees.id_mission = missions_autre_voyageur.id_mission;


-- on met à jour le motif avec les valeur labos1p5
UPDATE missions_realisees SET motif = 'Etude terrain' WHERE motif = 'etude';
UPDATE missions_realisees SET motif = 'Colloque-Congrès' WHERE motif = 'colloque';
UPDATE missions_realisees SET motif = 'Séminaire' WHERE motif = 'seminaire';
UPDATE missions_realisees SET motif = 'Enseignement' WHERE motif = 'enseignement';
UPDATE missions_realisees SET motif = 'Collaboration' WHERE motif = 'collaboration';
UPDATE missions_realisees SET motif = 'Visite' WHERE motif = 'visite';
UPDATE missions_realisees SET motif = 'Administration de la recherche' WHERE motif = 'adm';
UPDATE missions_realisees SET motif = 'Autre' WHERE motif = 'autre';


-- on met à jour le statut avec les valeur labos1p5
UPDATE missions_realisees SET statut_missionnaire = 'Chercheur.e-EC' WHERE statut_missionnaire = 'chercheur';
UPDATE missions_realisees SET statut_missionnaire = 'ITA' WHERE statut_missionnaire = 'ita';
UPDATE missions_realisees SET statut_missionnaire = 'Doc-Post doc' WHERE statut_missionnaire = 'doc-post-doc';
UPDATE missions_realisees SET statut_missionnaire = 'Personne invitée' WHERE statut_missionnaire = 'invite';

DROP TABLE IF EXISTS trajets_realises;
select 'creation trajets_realises';
CREATE TABLE trajets_realises AS 
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire as statut, motif, entite, nb_etapes, etape_destination,
      1 as num_etape, date_depart_1 as date_depart, ville_depart_1 as ville_depart, ville_arrivee_1 as ville_arrivee, 
      lat_ville_depart_1  as lat_ville_depart,  lon_ville_depart_1  as lon_ville_depart,
      lat_ville_arrivee_1 as lat_ville_arrivee, lon_ville_arrivee_1 as lon_ville_arrivee,
      mode_transport_1 as mode_transport, 
      case mode_transport_1 
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_1 as dist, emission_co2_1 as emission_co2
      from missions_realisees m1 where nb_etapes >= 1)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      2 as num_etape, date_depart_2, ville_depart_2, ville_arrivee_2, 
      lat_ville_depart_2 ,  lon_ville_depart_2,
      lat_ville_arrivee_2 , lon_ville_arrivee_2,
      mode_transport_2, 
      case mode_transport_2
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport, 
      dist_2, emission_co2_2
      from missions_realisees m1 where nb_etapes >= 2)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      3 as num_etape, date_depart_3, ville_depart_3, ville_arrivee_3, 
      lat_ville_depart_3 ,  lon_ville_depart_3,
      lat_ville_arrivee_3 , lon_ville_arrivee_3,
      mode_transport_3, 
      case mode_transport_3
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_3, emission_co2_3
      from missions_realisees m3 where nb_etapes >= 3)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      4 as num_etape, date_depart_4, ville_depart_4, ville_arrivee_4, 
      lat_ville_depart_4 ,  lon_ville_depart_4,
      lat_ville_arrivee_4 , lon_ville_arrivee_4,
      mode_transport_4, 
      case mode_transport_4
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_4, emission_co2_4
      from missions_realisees m4 where nb_etapes >= 4)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      5 as num_etape, date_depart_5, ville_depart_5, ville_arrivee_5, 
      lat_ville_depart_5 ,  lon_ville_depart_5,
      lat_ville_arrivee_5 , lon_ville_arrivee_5,
      mode_transport_5, 
      case mode_transport_5
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_5, emission_co2_5
      from missions_realisees m5 where nb_etapes >= 5)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      6 as num_etape, date_depart_6, ville_depart_6, ville_arrivee_6, 
      lat_ville_depart_6 ,  lon_ville_depart_6,
      lat_ville_arrivee_6 , lon_ville_arrivee_6,
      mode_transport_6, 
      case mode_transport_6
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_6, emission_co2_6
      from missions_realisees m6 where nb_etapes >= 6)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      7 as num_etape, date_depart_7, ville_depart_7, ville_arrivee_7,
      lat_ville_depart_7 ,  lon_ville_depart_7,
      lat_ville_arrivee_7 , lon_ville_arrivee_7, 
      mode_transport_7, 
      case mode_transport_7
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_7, emission_co2_7
      from missions_realisees m7 where nb_etapes >= 7)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      8 as num_etape, date_depart_8, ville_depart_8, ville_arrivee_8, 
      lat_ville_depart_8 ,  lon_ville_depart_8,
      lat_ville_arrivee_8 , lon_ville_arrivee_8,
      mode_transport_8, 
      case mode_transport_8
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_8, emission_co2_8
      from missions_realisees m8 where nb_etapes >= 8)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      9 as num_etape, date_depart_9, ville_depart_9, ville_arrivee_9, 
      lat_ville_depart_9 ,  lon_ville_depart_9,
      lat_ville_arrivee_9 , lon_ville_arrivee_9,
      mode_transport_9, 
      case mode_transport_9
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_9, emission_co2_9
      from missions_realisees m9 where nb_etapes >= 9)
   union
   (select id_mission, id_demandeur, demandeur_missionnaire, statut_missionnaire, motif, entite, nb_etapes, etape_destination,
      10 as num_etape, date_depart_10, ville_depart_10, ville_arrivee_10, 
      lat_ville_depart_10 ,  lon_ville_depart_10,
      lat_ville_arrivee_10 , lon_ville_arrivee_10,
      mode_transport_10, 
      case mode_transport_10
         when 'avion'              then 'avion'   
         when 'car'                then 'route'
         when 'covoiturage'        then 'route'
         when 'taxi'               then 'route'
         when 'vadministratif'     then 'route'
         when 'vlocation'          then 'route'
         when 'vpersonnel'         then 'route'
         when 'tgvfrance'          then 'train'
         when 'tgvinternational'   then 'train'
         when 'trainfrance'        then 'train'
         when 'traininternational' then 'train'
         when 'tgvinternational'   then 'train'
      end as type_transport,
      dist_10, emission_co2_10
      from missions_realisees m10 where nb_etapes >= 10)
   order by id_mission, num_etape
;


-- on met à jour missions_realisees en fonction de ce qui a ete calcule avec trajets_realises

-- dans les cas des missions à 1 ou 2 étapes, le lieu de la mission est la ville d'arrivée de la 1e étape
-- sinon, on regarde le lieu où l'agent a passé le plus de temps

UPDATE missions_realisees
   inner join 
   (select e1.id_mission, e1.num_etape
      from 
            (select id_mission, num_etape, 
               coalesce((select datediff(t2.date_depart , t1.date_depart) 
                  from trajets_realises t2 
                  where t1.id_mission = t2.id_mission 
                  and t2.num_etape=(t1.num_etape+1)),0) 
               as duree 
            from trajets_realises t1 
            where nb_etapes > 2) e1
      left join 
            (select id_mission, num_etape, 
               coalesce((select datediff(t2.date_depart , t1.date_depart) 
                  from trajets_realises t2 
                  where t1.id_mission = t2.id_mission 
                  and t2.num_etape=(t1.num_etape+1)),0) 
               as duree 
            from trajets_realises t1 
            where nb_etapes > 2) e2
      on e1.id_mission = e2.id_mission and e1.duree < e2.duree
      where e2.duree is null) as etape_dest
   on missions_realisees.id_mission = etape_dest.id_mission
   SET missions_realisees.etape_destination = etape_dest.num_etape
;

-- quand l'etape destination est en Idf, mais que la mission n'est pas complètement en IdF
-- on met la destination principale à la ville_arrivee de l'étape finale
-- c'est le cas des missions invitées

UPDATE missions_realisees
   inner join 
   (select id_mission, etape_destination
   from trajets_realises 
   where etape_destination = num_etape 
   and is_idf(lat_ville_arrivee, lon_ville_arrivee) 
   and id_mission not in (
      select id_mission 
         from trajets_realises
         where num_etape=1
         and is_idf(lat_ville_depart, lon_ville_depart)
         )) as etape_maj
   on missions_realisees.id_mission = etape_maj.id_mission
   SET missions_realisees.etape_destination = missions_realisees.nb_etapes
;

-- et on remet à jour les trajets
UPDATE trajets_realises t
   inner join missions_realisees m
   on t.id_mission = m.id_mission
   SET t.etape_destination = m.etape_destination;

DROP TABLE IF EXISTS bilan_missions;
select 'creation bilan_missions';
CREATE TABLE  bilan_missions AS 
select 
   year(date_depart_1) as annee,
   count(id_mission) as nb_mission,
   count(distinct id_demandeur) as nb_demandeurs,
   (select count(distinct nom_missionnaire) from missions_realisees m2 where entite != 'EXT' and demandeur_missionnaire and year(m1.date_depart_1) = year(m2.date_depart_1)) as nb_voyageurs_int,
   (select count(distinct nom_missionnaire) from missions_realisees m2 where entite != 'EXT' and (not demandeur_missionnaire) and year(m1.date_depart_1) = year(m2.date_depart_1)) as nb_voyageurs_ext,
   sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
      coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
      coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
      coalesce(dist_10,0)) as distance,
   round(sum(total_emission_co2)) as total_emission_co2,
   round(round(sum(total_emission_co2)) / 
      sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
         coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
         coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
         coalesce(dist_10,0)), 5) as coef_emission
from missions_realisees m1 where entite != 'EXT'
group by year(date_depart_1);


DROP TABLE IF EXISTS bilan_missions_ext;
select 'creation bilan_missions EXT';
CREATE TABLE  bilan_missions_ext AS 
select 
   year(date_depart_1) as annee,
   count(id_mission) as nb_mission,
   count(distinct id_demandeur) as nb_demandeurs,
   sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
      coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
      coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
      coalesce(dist_10,0)) as distance,
   round(sum(total_emission_co2)) as total_emission_co2,
   round(round(sum(total_emission_co2)) / 
      sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
         coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
         coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
         coalesce(dist_10,0)), 5) as coef_emission
from missions_realisees where entite = 'EXT'
group by year(date_depart_1);



DROP TABLE IF EXISTS bilan_trajets;
select 'creation bilan_trajets';
CREATE TABLE bilan_trajets AS 
select 
   year(date_depart) as annee,
   count(id_mission) as nb_trajet,
   count(distinct id_demandeur) as nb_demandeurs,
   sum(dist) as distance,
   round(sum(emission_co2)) as total_emission_co2,
   round(round(sum(emission_co2)) / sum(dist), 5) as coef_emission
from trajets_realises where entite != 'EXT'
group by year(date_depart);


source missions-bilan.sql;
