--
-- Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
-- florence.henry@obspm.fr
-- https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
--
-- This file is part of "OGM - Outil de gestion des missions".
-- 
-- OGM is free software: you can redistribute it and/or modify
-- it under the terms of the Affero GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- OGM is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with OGM.
--

-- Détermine si un lieu est en IDF ou non 
-- IDF = centre de Paris avec un rayon de 50km

DROP FUNCTION IF EXISTS is_idf;
DROP FUNCTION IF EXISTS distance_lieux;

delimiter $$

CREATE FUNCTION distance_lieux (lat1 real, lon1 real, lat2 real, lon2 real)
RETURNS real DETERMINISTIC
BEGIN
	SET @Rt := 6378137; -- Earth’s mean radius in meter

	SET @dLat := radians(lat1 - lat2);
	SET @dLong := radians(lon1 - lon2);

  	SET @a := sin(@dLat / 2) * sin(@dLat / 2) +
	    cos(radians(lat2)) * cos(radians(lat1)) *
	    sin(@dLong / 2) * sin(@dLong / 2);

	SET @c := 2 * atan2(sqrt(@a), sqrt(1 - @a));

  	RETURN round(@Rt * @c / 1000.0); -- distance in kilometer

END; $$

CREATE FUNCTION is_idf (lat real, lon real)
RETURNS boolean DETERMINISTIC
BEGIN
	SET @latParis := 48.85341;
	SET @lonParis := 2.3488;

  	SET @dist := distance_lieux (lat, lon, @latParis, @lonParis);

  	RETURN (@dist < 60);
END; $$


DELIMITER ;