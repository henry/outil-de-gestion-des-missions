--
-- Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
-- florence.henry@obspm.fr
-- https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
--
-- This file is part of "OGM - Outil de gestion des missions".
-- 
-- OGM is free software: you can redistribute it and/or modify
-- it under the terms of the Affero GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- OGM is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with OGM.
--

-- BILAN GENERAL


-- 1) Pour le LESIA en entier
-- a. Nombre de missions, nombre de trajets, Distance totale parcourue, CO2 total émis, 

-- 
-- bilan_missions
-- 

select * from bilan_missions;

-- 
-- bilan_trajets
-- 

select * from bilan_trajets;

-- b.  distance selon moyen de transport, proportions de chaque moyen de transport
-- f.  CO2 émis par moyen de transport, et proportions. Coefficient d'émission moyen (=CO2 total/distance totale)

-- 
-- mission_bilan_transport
-- 

drop table if exists mission_bilan_transport;
create table mission_bilan_transport as 
select 
	year(t.date_depart) as annee,
	count(*) as nb_trajets,
	sum(t.dist) as distance,
	type_transport as transport, 
	round(sum(t.dist) / b.distance  * 100,2) as ratio_distance,
	round(sum(t.emission_co2)) as total_emission_co2,
	round(sum(t.emission_co2)) / b.total_emission_co2 * 100 as ratio_emission,
	round(round(sum(t.emission_co2)) / sum(t.dist), 5) as coef_emission
from trajets_realises t 
left outer join bilan_trajets b on b.annee = year(t.date_depart)
where entite != 'EXT'
group by year(t.date_depart), transport
order by annee, distance desc;

-- 
-- mission_bilan_route
-- 

-- idem que précédemment, mais que pour la route, en séparant les types de véhicules

drop table if exists mission_bilan_route;
create table mission_bilan_route as 
select 
   year(t.date_depart) as annee,
   count(*) as nb_trajets,
   sum(t.dist) as distance,
   mode_transport as transport, 
   round(sum(t.dist) / 
      (select distance from mission_bilan_transport where annee = year(t.date_depart) and transport = 'route') 
      * 100,2) as ratio_distance,
   round(sum(t.emission_co2)) as total_emission_co2,
   round(sum(t.emission_co2)) / 
      (select total_emission_co2 from mission_bilan_transport where annee = year(t.date_depart) and transport = 'route') 
      * 100 as ratio_emission,
   round(round(sum(t.emission_co2)) / sum(t.dist), 5) as coef_emission
from trajets_realises t 
where entite != 'EXT'
and type_transport = 'route'
group by year(t.date_depart), transport
order by annee, distance desc;

-- 
-- mission_bilan_train
-- 

-- idem que précédemment, mais que pour la train, en séparant les types de trains

drop table if exists mission_bilan_train;
create table mission_bilan_train as 
select 
   year(t.date_depart) as annee,
   count(*) as nb_trajets,
   sum(t.dist) as distance,
   mode_transport as transport, 
   round(sum(t.dist) / 
      (select distance from mission_bilan_transport where annee = year(t.date_depart) and transport = 'train') 
      * 100,2) as ratio_distance,
   round(sum(t.emission_co2)) as total_emission_co2,
   round(sum(t.emission_co2)) / 
      (select total_emission_co2 from mission_bilan_transport where annee = year(t.date_depart) and transport = 'train') 
      * 100 as ratio_emission,
   round(round(sum(t.emission_co2)) / sum(t.dist), 5) as coef_emission
from trajets_realises t 
where entite != 'EXT'
and type_transport = 'train'
group by year(t.date_depart), transport
order by annee, distance desc;


-- c. Distribution (histogramme) des distances parcourues par mission, tous moyens de transport confondus. Valeur médiane

-- 
-- mission_hist_distance
-- 

drop table if exists mission_hist_distance_data;
create table mission_hist_distance_data as 
select
	year(date_depart_1) as annee,
	id_mission, 
	coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
		coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
		coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
		coalesce(dist_10,0) as distance
from missions_realisees
where entite != 'EXT'
order by annee, distance desc;

drop table if exists mission_hist_distance ;
create table mission_hist_distance as 
select annee, 500 as wbin, round(distance/500,0)*500 as bin, count(*) as count 
	from mission_hist_distance_data 
	group by annee, bin;


-- calcul de la médiane

-- 
-- mission_mediane_distance
-- 

drop table if exists mission_mediane_distance;
create table mission_mediane_distance as 
SELECT annee, AVG(distance) as "mediane"
FROM 
(
  SELECT annee, distance,
      (SELECT count(*) FROM missions_realisees t2 WHERE year(t2.date_depart_1) = t3.annee AND entite != 'EXT') as ct,
      seq,
      (SELECT count(*) FROM missions_realisees t2 WHERE year(t2.date_depart_1) < t3.annee AND entite != 'EXT') as delta
    FROM (SELECT annee, distance, @rownum := @rownum + 1 as seq
          FROM (SELECT year(date_depart_1) as annee,  coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
			coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
			coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
			coalesce(dist_10,0) as distance
			FROM missions_realisees 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			ORDER BY annee, distance) t1 
          ORDER BY annee, seq
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee
ORDER BY annee;


-- d. Distribution (histogramme) des distances parcourues par trajet, tous moyens de transport confondus. Valeur médiane

-- 
-- trajet_hist_distance
-- 

drop table if exists trajet_hist_data;
create table trajet_hist_data as 
select
	year(date_depart) as annee,
	id_mission, num_etape, 
	cast(dist as unsigned integer) as distance,
	cast(emission_co2 as decimal) as emission_co2
from trajets_realises
where entite != 'EXT'
order by annee, distance desc;


drop table if exists trajet_hist_distance;
create table trajet_hist_distance as 
	select annee, 200 as wbin, round(distance/200,0)*200 as bin, count(*) as count, 
	sum(emission_co2) as emission_co2, sum(distance) as sum_distance
	from trajet_hist_data 
	group by annee, bin;



-- calcul de la médiane

-- 
-- trajet_mediane_distance
-- 

drop table if exists trajet_mediane_distance;
create table trajet_mediane_distance as 
SELECT annee, AVG(distance) as "mediane"
FROM 
(
  SELECT annee, distance,
      (SELECT count(*) FROM trajets_realises t2 
      	WHERE year(t2.date_depart) = t3.annee AND entite != 'EXT') as ct,
      seq,
      (SELECT count(*) FROM trajets_realises t2 
      	WHERE year(t2.date_depart) < t3.annee AND entite != 'EXT') as delta
    FROM (SELECT annee, distance, @rownum := @rownum + 1 as seq
          FROM (SELECT year(date_depart) as annee,  cast(dist as unsigned integer) as distance
			FROM trajets_realises 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			ORDER BY annee, distance) t1 
          ORDER BY annee, seq
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee
ORDER BY annee;


-- e. Distribution (histogramme) des distances parcourues par trajet et pour chaque moyen de transport. Valeur médiane

-- 
-- trajet_hist_distance_transport
-- 

drop table if exists trajet_hist_distance_transport_data;
create table trajet_hist_distance_transport_data as 
select
	year(date_depart) as annee,
	id_mission, num_etape, 
	type_transport as transport, 
	cast(dist as unsigned integer) as distance,
	case type_transport when 'avion' then 500 else 100 end as wbin
from trajets_realises
where entite != 'EXT'
order by annee, distance desc;


drop table if exists trajet_hist_distance_transport;
create table trajet_hist_distance_transport as 
	select annee, wbin,
		transport, round(distance/wbin,0)*wbin as bin, count(*) as count 
	from trajet_hist_distance_transport_data 
	group by annee, transport, bin;

-- calcul de la médiane

-- 
-- trajet_mediane_distance_transport
-- 

drop table if exists trajet_mediane_distance_transport;
create table trajet_mediane_distance_transport as 
SELECT annee, transport, AVG(distance) as "mediane"
FROM 
(
  SELECT annee, transport, distance,
      (SELECT count(*) FROM trajets_realises t2 WHERE 
      	(year(t2.date_depart) = t3.annee and t2.type_transport = t3.transport) AND entite != 'EXT') as ct,
      seq,
      (SELECT count(*) FROM trajets_realises t2 WHERE 
      	(year(t2.date_depart) < t3.annee OR t2.type_transport < t3.transport) AND entite != 'EXT') as delta
    FROM (SELECT annee, transport, distance, @rownum := @rownum + 1 as seq
          FROM (SELECT 
          		year(date_depart) as annee,  
          		cast(dist as unsigned integer) as distance,
				type_transport as transport
			FROM trajets_realises 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			ORDER BY annee, transport, distance) t1 
          ORDER BY annee, transport, seq
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee, transport
ORDER BY annee, transport;


-- tracer la distribution cumulative du CO2 en fonction de la longueur des trajets 
-- (et en orientant l'axe x des plus grandes valeurs du trajet aux plus petites).
-- TOOD : faire l'inverse

-- 
-- trajet_cumul_co2_dist
-- 

drop table if exists trajet_cumul_co2_dist;
create table trajet_cumul_co2_dist as 
select distinct annee,  bin as distance, wbin,
	(select sum(emission_co2) from trajet_hist_distance t2 where t2.bin <= t1.bin and t2.annee = t1.annee ) as cumul_co2, 
	round((select sum(emission_co2) from trajet_hist_distance t5 where t5.bin >= t1.bin and t5.annee = t1.annee)/
		(select sum(emission_co2) from trajet_hist_distance t6 where t6.annee = t1.annee)*100, 0)  as ratio_co2,
	(select sum(emission_co2) from trajet_hist_distance t3 where t3.bin = t1.bin and t3.annee = t1.annee) as sum_co2,
	(select count from trajet_hist_distance t4 where t4.bin = t1.bin and t4.annee = t1.annee) as count
	from trajet_hist_distance t1  
	order by annee, bin, wbin desc;


drop table if exists trajet_cumul_co2_nb;
create table trajet_cumul_co2_nb as 
select
	annee, num, emission_co2,
	(select sum(emission_co2) from 
		(SELECT year(date_depart) as annee, @rownuma := @rownuma + 1 as num ,cast(emission_co2 as decimal(10,1)) as emission_co2 
			FROM trajets_realises 
			CROSS JOIN (SELECT @rownuma := 0) x
			WHERE entite != 'EXT'
			ORDER BY cast(emission_co2 as decimal(10,1)) desc) t1b
		where t1.num >= t1b.num and t1.annee = t1b.annee ) as cumul_co2,
	round((select sum(emission_co2) from 
		(SELECT year(date_depart) as annee, @rownumb := @rownumb + 1 as num ,cast(emission_co2 as decimal(10,1)) as emission_co2 
			FROM trajets_realises 
			CROSS JOIN (SELECT @rownumb := 0) x
			WHERE entite != 'EXT'
			ORDER BY cast(emission_co2 as decimal(10,1)) desc) t1c
		where t1.num >= t1c.num and t1.annee = t1c.annee )/
	(select sum(emission_co2) from trajets_realises t2 WHERE entite != 'EXT' and t1.annee = year(t2.date_depart))*100,2) as ratio_co2
	FROM
		(SELECT year(date_depart) as annee, @rownum := @rownum + 1 as num ,cast(emission_co2 as decimal(10,1)) as emission_co2 
			FROM trajets_realises 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			ORDER BY cast(emission_co2 as decimal(10,1)) desc) t1
	ORDER by annee, num
		;




-- g. Distributions (histogramme et cumulative) anonymisée de la distance parcourue et du CO2 émis par agent ayant voyagé au moins 1 fois. Valeurs médianes et moyennes
-- par agent ayant voyagé.

-- 
-- mission_hist_agent
-- 

drop table if exists mission_hist_agent_data;
create table mission_hist_agent_data as 
select id_demandeur,
	year(date_depart_1) as annee,
	count(distinct id_mission) as nb_missions,
	sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
	 		coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
	 		coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
	 		coalesce(dist_10,0)) as distance,
	round(sum(total_emission_co2),1) as total_emission_co2
from missions_realisees
where entite != 'EXT'
group by annee, id_demandeur
order by annee, distance desc;

drop table if exists mission_hist_agent_dist;
create table mission_hist_agent_dist as 
	select annee, 500 as wbin, round(distance/500,0)*500 as bin, count(*) as count 
	from mission_hist_agent_data 
	group by annee, bin;


drop table if exists mission_hist_agent_co2;
create table mission_hist_agent_co2 as 
	select annee, 200 as wbin, round(total_emission_co2/100,0)*100 as bin, count(*) as count 
	from mission_hist_agent_data 
	group by annee, bin;


drop table if exists mission_hist_agent_nb;
create table mission_hist_agent_nb as 
	select annee, 1 as wbin, nb_missions as bin, count(*) as count 
	from mission_hist_agent_data 
	group by annee, bin;


-- graphes cumulatifs

drop table if exists mission_cumul_agent_dist;
create table mission_cumul_agent_dist as 
select distinct annee,  distance, 
	(select sum(distance) from mission_hist_agent_data m2 where m2.distance >= m1.distance and m2.annee = m1.annee ) as cumul_distance, 
	round((select sum(distance) from mission_hist_agent_data m5 where m5.distance >= m1.distance and m5.annee = m1.annee)/
		(select sum(distance) from mission_hist_agent_data m6 where m6.annee = m1.annee)*100, 0)  as ratio_distance, 
	(select count(id_demandeur) from mission_hist_agent_data m3 where m3.distance >= m1.distance and m3.annee = m1.annee)  as cumul_agent, 
	(select count(id_demandeur) from mission_hist_agent_data m3 where m3.distance <= m1.distance and m3.annee = m1.annee)  as reste_agent, 
	round((select count(id_demandeur) from mission_hist_agent_data m3 where m3.distance >= m1.distance and m3.annee = m1.annee)/
		(select count(id_demandeur) from mission_hist_agent_data m4 where m4.annee = m1.annee)*100, 0)  as ratio_agent 
	from mission_hist_agent_data m1  
	order by annee, distance desc;


drop table if exists mission_cumul_agent_co2;
create table mission_cumul_agent_co2 as 
select distinct annee,  total_emission_co2, 
	(select sum(total_emission_co2) from mission_hist_agent_data m2 where m2.total_emission_co2 >= m1.total_emission_co2 and m2.annee = m1.annee ) as cumul_co2, 
	round((select sum(total_emission_co2) from mission_hist_agent_data m5 where m5.total_emission_co2 >= m1.total_emission_co2 and m5.annee = m1.annee)/
		(select sum(total_emission_co2) from mission_hist_agent_data m6 where m6.annee = m1.annee)*100, 0)  as ratio_co2, 
	(select count(id_demandeur) from mission_hist_agent_data m3 where m3.total_emission_co2 >= m1.total_emission_co2 and m3.annee = m1.annee) as cumul_agent ,
	(select count(id_demandeur) from mission_hist_agent_data m3 where m3.total_emission_co2 <= m1.total_emission_co2 and m3.annee = m1.annee) as reste_agent ,
	round((select count(id_demandeur) from mission_hist_agent_data m3 where m3.total_emission_co2 >= m1.total_emission_co2 and m3.annee = m1.annee)/
		(select count(id_demandeur) from mission_hist_agent_data m4 where m4.annee = m1.annee)*100, 0)  as ratio_agent 
	from mission_hist_agent_data m1  
	order by annee, total_emission_co2 desc;


-- calcul de la médiane

-- 
-- mission_mediane_distance_agent
-- 

drop table if exists mission_mediane_distance_agent;
create table mission_mediane_distance_agent as 
SELECT annee, AVG(distance) as "mediane"
FROM 
(
  SELECT annee, distance,
      (SELECT count(distinct id_demandeur) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) = t3.annee)
      		AND entite != 'EXT') as ct,
      seq,
      (SELECT count(distinct id_demandeur) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) < t3.annee )
      		AND entite != 'EXT') as delta
    FROM (SELECT annee, id_demandeur, distance, @rownum := @rownum + 1 as seq
          FROM (SELECT year(date_depart_1) as annee,  id_demandeur,
          	sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
	 		coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
	 		coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
	 		coalesce(dist_10,0)) as distance
			FROM missions_realisees 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			GROUP BY annee, id_demandeur
			ORDER BY annee, distance) t1 
          ORDER BY annee, seq
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee
ORDER BY annee;

-- 
-- mission_mediane_co2_agent
-- 

drop table if exists mission_mediane_co2_agent;
create table mission_mediane_co2_agent as 
SELECT annee, AVG(total_emission_co2) as "mediane"
FROM 
(
  SELECT annee, total_emission_co2,
      (SELECT count(distinct id_demandeur) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) = t3.annee)
      		AND entite != 'EXT') as ct,
      seq,
      (SELECT count(distinct id_demandeur) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) < t3.annee )
      		AND entite != 'EXT') as delta
    FROM (SELECT annee, id_demandeur, total_emission_co2, @rownum := @rownum + 1 as seq
          FROM (SELECT year(date_depart_1) as annee,  id_demandeur,
          	sum(total_emission_co2) as total_emission_co2
			FROM missions_realisees 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			GROUP BY annee, id_demandeur
			ORDER BY annee, total_emission_co2) t1 
          ORDER BY annee, seq
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee
ORDER BY annee;


-- 
-- mission_mediane_nb_missions_agent
-- 

drop table if exists mission_mediane_nb_missions_agent;
create table mission_mediane_nb_missions_agent as 
SELECT annee, AVG(nb_missions) as "mediane"
FROM 
(
  SELECT annee, nb_missions,
      (SELECT count(distinct id_demandeur) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) = t3.annee)
      		AND entite != 'EXT') as ct,
      seq,
      (SELECT count(distinct id_demandeur) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) < t3.annee )
      		AND entite != 'EXT') as delta
    FROM (SELECT annee, id_demandeur, nb_missions, @rownum := @rownum + 1 as seq
          FROM (SELECT year(date_depart_1) as annee,  id_demandeur,
          	count(distinct id_mission) as nb_missions
			FROM missions_realisees 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			GROUP BY annee, id_demandeur
			ORDER BY annee, nb_missions) t1 
          ORDER BY annee, nb_missions
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee
ORDER BY annee;

-- Idem pour les entités

-- 
-- mission_hist_entite
-- 

drop table if exists mission_hist_entite_data;
create table mission_hist_entite_data as 
select entite,
	year(date_depart_1) as annee,
	count(distinct id_mission) as nb_missions,
	sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
	 		coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
	 		coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
	 		coalesce(dist_10,0)) as distance,
	round(sum(total_emission_co2),1) as total_emission_co2
from missions_realisees
where entite != 'EXT'
group by annee, entite
order by annee, distance desc;

drop table if exists mission_hist_entite_dist;
create table mission_hist_entite_dist as 
	select annee, 500 as wbin, round(distance/500,0)*500 as bin, count(*) as count 
	from mission_hist_entite_data 
	group by annee, bin;


drop table if exists mission_hist_entite_co2;
create table mission_hist_entite_co2 as 
	select annee, 200 as wbin, round(total_emission_co2/100,0)*100 as bin, count(*) as count 
	from mission_hist_entite_data 
	group by annee, bin;


drop table if exists mission_hist_entite_nb;
create table mission_hist_entite_nb as 
	select annee, 1 as wbin, nb_missions as bin, count(*) as count 
	from mission_hist_entite_data 
	group by annee, bin;



-- 
-- mission_mediane_distance_entite
-- 

drop table if exists mission_mediane_distance_entite;
create table mission_mediane_distance_entite as 
SELECT annee, AVG(distance) as "mediane"
FROM 
(
  SELECT annee, distance,
      (SELECT count(distinct entite) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) = t3.annee)
      		AND entite != 'EXT') as ct,
      seq,
      (SELECT count(distinct entite) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) < t3.annee )
      		AND entite != 'EXT') as delta
    FROM (SELECT annee, entite, distance, @rownum := @rownum + 1 as seq
          FROM (SELECT year(date_depart_1) as annee,  entite,
          	sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
	 		coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
	 		coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
	 		coalesce(dist_10,0)) as distance
			FROM missions_realisees 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			GROUP BY annee, entite
			ORDER BY annee, distance) t1 
          ORDER BY annee, seq
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee
ORDER BY annee;



-- 
-- mission_mediane_co2_entite
-- 

drop table if exists mission_mediane_co2_entite;
create table mission_mediane_co2_entite as 
SELECT annee, AVG(total_emission_co2) as "mediane"
FROM 
(
  SELECT annee, total_emission_co2,
      (SELECT count(distinct entite) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) = t3.annee)
      		AND entite != 'EXT') as ct,
      seq,
      (SELECT count(distinct entite) FROM missions_realisees t2 
      		WHERE (year(t2.date_depart_1) < t3.annee )
      		AND entite != 'EXT') as delta
    FROM (SELECT annee, entite, total_emission_co2, @rownum := @rownum + 1 as seq
          FROM (SELECT year(date_depart_1) as annee,  entite,
          	sum(total_emission_co2) as total_emission_co2
			FROM missions_realisees 
			CROSS JOIN (SELECT @rownum := 0) x
			WHERE entite != 'EXT'
			GROUP BY annee, entite
			ORDER BY annee, total_emission_co2) t1 
          ORDER BY annee, seq
        ) t3 
    HAVING (ct%2 = 0 and seq-delta between floor((ct+1)/2) and floor((ct+1)/2) +1)
      or (ct%2 <> 0 and seq-delta = (ct+1)/2)
) T
GROUP BY annee
ORDER BY annee;



-- 
-- 2) Pour chaque agent
-- - Nb de missions
-- - Distance totale parcourue, distances selon moyen de transport.
-- - CO2 total émis et coefficient d'émission moyen. CO2 par moyen de transport. Rappel des résultats 1f et 1g au niveau du labo

-- 
-- mission_bilan_agent
-- 


drop table if exists mission_bilan_agent;
create table mission_bilan_agent as 
	SELECT year(date_depart_1) as annee, 
		id_demandeur,  'tout' as demandeur_missionnaire,
		count(id_mission) as nb_mission,
		sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
			coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
			coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
			coalesce(dist_10,0)) as distance, 
		round(sum(total_emission_co2),3) as total_emission_co2,
		(select round(coalesce(sum(coalesce(ta.dist,0)),0),3) from trajets_realises ta 
			where ta.id_demandeur = m.id_demandeur 
			and year(ta.date_depart) = year(m.date_depart_1)
			and ta.type_transport = 'avion') as dist_avion,
		(select round(coalesce(sum(coalesce(ta2.emission_co2,0)),0),3) from trajets_realises ta2 
			where ta2.id_demandeur = m.id_demandeur 
			and year(ta2.date_depart) = year(m.date_depart_1)
			and ta2.type_transport = 'avion') as emission_avion,
		(select round(coalesce(sum(coalesce(tt.dist,0)),0),3) from trajets_realises tt 
			where tt.id_demandeur = m.id_demandeur 
			and year(tt.date_depart) = year(m.date_depart_1)
			and tt.type_transport = 'train') as dist_train,
		(select round(coalesce(sum(coalesce(tt2.emission_co2,0)),0),3) from trajets_realises tt2 
			where tt2.id_demandeur = m.id_demandeur 
			and year(tt2.date_depart) = year(m.date_depart_1)
			and tt2.type_transport = 'train') as emission_train,
		(select round(coalesce(sum(coalesce(tr.dist,0)),0),3) from trajets_realises tr 
			where tr.id_demandeur = m.id_demandeur 
			and year(tr.date_depart) = year(m.date_depart_1)
			and tr.type_transport = 'route') as dist_route,
		(select round(coalesce(sum(coalesce(tr2.emission_co2,0)),0),3) from trajets_realises tr2 
			where tr2.id_demandeur = m.id_demandeur 
			and year(tr2.date_depart) = year(m.date_depart_1)
			and tr2.type_transport = 'route') as emission_route
	FROM missions_realisees m
	GROUP BY annee, id_demandeur
UNION
	SELECT year(date_depart_1) as annee, 
		id_demandeur,  'oui' as demandeur_missionnaire,
		count(id_mission) as nb_mission,
		sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
			coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
			coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
			coalesce(dist_10,0)) as distance, 
		round(sum(total_emission_co2),3) as total_emission_co2,
		(select round(coalesce(sum(coalesce(ta.dist,0)),0),3) from trajets_realises ta 
			where ta.id_demandeur = m.id_demandeur and demandeur_missionnaire and entite != 'EXT'
			and year(ta.date_depart) = year(m.date_depart_1)
			and ta.type_transport = 'avion') as dist_avion,
		(select round(coalesce(sum(coalesce(ta2.emission_co2,0)),0),3) from trajets_realises ta2 
			where ta2.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite != 'EXT'
			and year(ta2.date_depart) = year(m.date_depart_1)
			and ta2.type_transport = 'avion') as emission_avion,
		(select round(coalesce(sum(coalesce(tt.dist,0)),0),3) from trajets_realises tt 
			where tt.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite != 'EXT'
			and year(tt.date_depart) = year(m.date_depart_1)
			and tt.type_transport = 'train') as dist_train,
		(select round(coalesce(sum(coalesce(tt2.emission_co2,0)),0),3) from trajets_realises tt2 
			where tt2.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite != 'EXT'
			and year(tt2.date_depart) = year(m.date_depart_1)
			and tt2.type_transport = 'train') as emission_train,
		(select round(coalesce(sum(coalesce(tr.dist,0)),0),3) from trajets_realises tr 
			where tr.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite != 'EXT'
			and year(tr.date_depart) = year(m.date_depart_1)
			and tr.type_transport = 'route') as dist_route,
		(select round(coalesce(sum(coalesce(tr2.emission_co2,0)),0),3) from trajets_realises tr2 
			where tr2.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite != 'EXT'
			and year(tr2.date_depart) = year(m.date_depart_1)
			and tr2.type_transport = 'route') as emission_route
	FROM missions_realisees m
	WHERE demandeur_missionnaire and entite != 'EXT'
	GROUP BY annee, id_demandeur
UNION
	SELECT year(date_depart_1) as annee, 
		id_demandeur,  'ext' as demandeur_missionnaire,
		count(id_mission) as nb_mission,
		sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
			coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
			coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
			coalesce(dist_10,0)) as distance, 
		round(sum(total_emission_co2),3) as total_emission_co2,
		(select round(coalesce(sum(coalesce(ta.dist,0)),0),3) from trajets_realises ta 
			where ta.id_demandeur = m.id_demandeur and demandeur_missionnaire and entite = 'EXT'
			and year(ta.date_depart) = year(m.date_depart_1)
			and ta.type_transport = 'avion') as dist_avion,
		(select round(coalesce(sum(coalesce(ta2.emission_co2,0)),0),3) from trajets_realises ta2 
			where ta2.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite = 'EXT'
			and year(ta2.date_depart) = year(m.date_depart_1)
			and ta2.type_transport = 'avion') as emission_avion,
		(select round(coalesce(sum(coalesce(tt.dist,0)),0),3) from trajets_realises tt 
			where tt.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite = 'EXT'
			and year(tt.date_depart) = year(m.date_depart_1)
			and tt.type_transport = 'train') as dist_train,
		(select round(coalesce(sum(coalesce(tt2.emission_co2,0)),0),3) from trajets_realises tt2 
			where tt2.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite = 'EXT'
			and year(tt2.date_depart) = year(m.date_depart_1)
			and tt2.type_transport = 'train') as emission_train,
		(select round(coalesce(sum(coalesce(tr.dist,0)),0),3) from trajets_realises tr 
			where tr.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite = 'EXT'
			and year(tr.date_depart) = year(m.date_depart_1)
			and tr.type_transport = 'route') as dist_route,
		(select round(coalesce(sum(coalesce(tr2.emission_co2,0)),0),3) from trajets_realises tr2 
			where tr2.id_demandeur = m.id_demandeur  and demandeur_missionnaire and entite = 'EXT'
			and year(tr2.date_depart) = year(m.date_depart_1)
			and tr2.type_transport = 'route') as emission_route
	FROM missions_realisees m
	WHERE demandeur_missionnaire and entite = 'EXT'
	GROUP BY annee, id_demandeur
UNION
	SELECT year(date_depart_1) as annee, 
		id_demandeur,  'non' as demandeur_missionnaire,
		count(id_mission) as nb_mission,
		sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
			coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
			coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
			coalesce(dist_10,0)) as distance, 
		round(sum(total_emission_co2),3) as total_emission_co2,
		(select round(coalesce(sum(coalesce(ta.dist,0)),0),3) from trajets_realises ta 
			where ta.id_demandeur = m.id_demandeur  and not demandeur_missionnaire
			and year(ta.date_depart) = year(m.date_depart_1)
			and ta.type_transport = 'avion') as dist_avion,
		(select round(coalesce(sum(coalesce(ta2.emission_co2,0)),0),3) from trajets_realises ta2 
			where ta2.id_demandeur = m.id_demandeur  and not demandeur_missionnaire
			and year(ta2.date_depart) = year(m.date_depart_1)
			and ta2.type_transport = 'avion') as emission_avion,
		(select round(coalesce(sum(coalesce(tt.dist,0)),0),3) from trajets_realises tt 
			where tt.id_demandeur = m.id_demandeur  and not demandeur_missionnaire
			and year(tt.date_depart) = year(m.date_depart_1)
			and tt.type_transport = 'train') as dist_train,
		(select round(coalesce(sum(coalesce(tt2.emission_co2,0)),0),3) from trajets_realises tt2 
			where tt2.id_demandeur = m.id_demandeur  and not demandeur_missionnaire
			and year(tt2.date_depart) = year(m.date_depart_1)
			and tt2.type_transport = 'train') as emission_train,
		(select round(coalesce(sum(coalesce(tr.dist,0)),0),3) from trajets_realises tr 
			where tr.id_demandeur = m.id_demandeur  and not demandeur_missionnaire
			and year(tr.date_depart) = year(m.date_depart_1)
			and tr.type_transport = 'route') as dist_route,
		(select round(coalesce(sum(coalesce(tr2.emission_co2,0)),0),3) from trajets_realises tr2 
			where tr2.id_demandeur = m.id_demandeur  and not demandeur_missionnaire
			and year(tr2.date_depart) = year(m.date_depart_1)
			and tr2.type_transport = 'route') as emission_route
	FROM missions_realisees m
	WHERE not demandeur_missionnaire 
	GROUP BY annee, id_demandeur
ORDER BY annee, id_demandeur, demandeur_missionnaire
;

-- 
-- 3) Idem que 2) pour chaque entité dépensière. 

-- 
-- mission_bilan_entite
-- 

drop table if exists mission_bilan_entite;
create table mission_bilan_entite as 
SELECT year(date_depart_1) as annee, 
	entite, 
	count(id_mission) as nb_mission,
	sum(coalesce(dist_1,0) + coalesce(dist_2,0) + coalesce(dist_3,0) + 
		coalesce(dist_4,0) + coalesce(dist_5,0) + coalesce(dist_6,0) + 
		coalesce(dist_7,0) + coalesce(dist_8,0) + coalesce(dist_9,0) + 
		coalesce(dist_10,0)) as distance, 
	round(sum(total_emission_co2),3) as total_emission_co2,
	(select coalesce(sum(coalesce(ta.dist,0)),0) from trajets_realises ta 
		where ta.entite = m.entite 
		and year(ta.date_depart) = year(m.date_depart_1)
		and ta.type_transport = 'avion') as dist_avion,
	(select coalesce(sum(coalesce(ta2.emission_co2,0)),0) from trajets_realises ta2 
		where ta2.entite = m.entite 
		and year(ta2.date_depart) = year(m.date_depart_1)
		and ta2.type_transport = 'avion') as emission_avion,
	(select coalesce(sum(coalesce(tt.dist,0)),0) from trajets_realises tt 
		where tt.entite = m.entite 
		and year(tt.date_depart) = year(m.date_depart_1)
		and tt.type_transport = 'train') as dist_train,
	(select coalesce(sum(coalesce(tt2.emission_co2,0)),0) from trajets_realises tt2 
		where tt2.entite = m.entite 
		and year(tt2.date_depart) = year(m.date_depart_1)
		and tt2.type_transport = 'train') as emission_train,
	(select coalesce(sum(coalesce(tr.dist,0)),0) from trajets_realises tr 
		where tr.entite = m.entite 
		and year(tr.date_depart) = year(m.date_depart_1)
		and tr.type_transport = 'route') as dist_route,
	(select coalesce(sum(coalesce(tr2.emission_co2,0)),0) from trajets_realises tr2 
		where tr2.entite = m.entite 
		and year(tr2.date_depart) = year(m.date_depart_1)
		and tr2.type_transport = 'route') as emission_route
FROM missions_realisees m
WHERE entite != 'EXT'
GROUP BY annee, entite
ORDER BY annee, distance desc;

--
-- mission_dist_temps
--

-- Distribution des missions dans l'année

drop table if exists mission_hist_temps;
create table mission_hist_temps as
	SELECT year(date_depart_1) as annee, 
		count(*) as nombre,
		floor((week(date_depart_1)+1)/2) as quinzaine,
		DATE_ADD(concat(year(date_depart_1),'-01-01'), interval floor((week(date_depart_1)+1)/2)*14  day) as date_quinzaine
	FROM missions_realisees 
	GROUP BY annee, quinzaine, date_quinzaine
	ORDER BY annee, quinzaine, date_quinzaine;


-- 
-- Trajets de moins de 1000 km en avion
-- 

DROP TABLE IF EXISTS trajets_1000km;
CREATE TABLE trajets_1000km AS 
SELECT 
	year(date_depart) as annee, 
	count(*) as count,
	1000 as dist_max,
	sum(dist) as distance,
	round(sum(emission_co2),2) as emission_co2,
	(ville_depart like '%Paris%' or ville_arrivee like '%Paris%') as de_ou_vers_paris
	FROM trajets_realises 
	WHERE type_transport ='avion' and dist < 1000 
	AND entite != 'EXT'
	GROUP BY annee, de_ou_vers_paris
UNION
SELECT 
	year(date_depart) as annee, 
	count(*) as count,
	750 as dist_max,
	sum(dist) as distance,
	round(sum(emission_co2),2) as emission_co2,
	(ville_depart like '%Paris%' or ville_arrivee like '%Paris%') as de_ou_vers_paris
	FROM trajets_realises 
	WHERE type_transport ='avion' and dist < 750 
	AND entite != 'EXT'
	GROUP BY annee, de_ou_vers_paris
UNION
SELECT 
	year(date_depart) as annee, 
	count(*) as count,
	500 as dist_max,
	sum(dist) as distance,
	round(sum(emission_co2),2) as emission_co2,
	(ville_depart like '%Paris%' or ville_arrivee like '%Paris%') as de_ou_vers_paris
	FROM trajets_realises 
	WHERE type_transport ='avion' and dist < 500 
	AND entite != 'EXT'
	GROUP BY annee, de_ou_vers_paris
	;

-- 
-- Bilan géographique 
-- 

drop table if exists mission_carte_destination;
create table mission_carte_destination
select
	year(date_depart_1) as annee,
	count(*) as count,
	round(sum(total_emission_co2),2) as total_emission_co2,
	(select 
			case when is_idf(lat_ville_arrivee, lon_ville_arrivee) then
				ville_depart
			else 
				ville_arrivee
			end
			as destination from trajets_realises t 
			where t.num_etape = t.etape_destination 
			and t.id_mission = m.id_mission
			and entite != 'EXT'
	) as destination,
	(select case when is_idf(lat_ville_arrivee, lon_ville_arrivee) then
				lat_ville_depart
			else 
				lat_ville_arrivee
			end
			as latitude from trajets_realises t 
			where t.num_etape = t.etape_destination 
			and t.id_mission = m.id_mission
			and entite != 'EXT'
	) as latitude,
	(select case when is_idf(lat_ville_arrivee, lon_ville_arrivee) then
				lon_ville_depart
			else 
				lon_ville_arrivee
			end
			as longitude from trajets_realises t 
			where t.num_etape = t.etape_destination 
			and t.id_mission = m.id_mission
			and entite != 'EXT'
	) as longitude,
   '   ' as continent,
	(select type_transport 
		from trajets_realises t 
		where t.id_mission = m.id_mission 
		and entite != 'EXT'
		order by cast(dist as signed) desc limit 1) as transport
from missions_realisees m
where not (coalesce(is_idf(lat_ville_depart_1, lon_ville_depart_1),true) -- on exclut les missions entièrement en IdF
	and coalesce(is_idf(lat_ville_arrivee_1, lon_ville_arrivee_1),true)  -- pour les missions avec une seule étape
	and coalesce(is_idf(lat_ville_depart_2, lon_ville_depart_2),true) 
	and coalesce(is_idf(lat_ville_depart_3, lon_ville_depart_3) ,true) 
	and coalesce(is_idf(lat_ville_depart_4, lon_ville_depart_4),true) 
	and coalesce(is_idf(lat_ville_depart_5, lon_ville_depart_5) ,true) 
	and coalesce(is_idf(lat_ville_depart_6, lon_ville_depart_6),true) 
	and coalesce(is_idf(lat_ville_depart_7, lon_ville_depart_7) ,true) 
	and coalesce(is_idf(lat_ville_depart_8, lon_ville_depart_8),true) 
	and coalesce(is_idf(lat_ville_depart_9, lon_ville_depart_9) ,true) 
	and coalesce(is_idf(lat_ville_depart_10, lon_ville_depart_10),true) )
	and entite != 'EXT'
group by annee, latitude, longitude, transport
order by annee, destination;


-- pour celle là on fait une vue car le script de mise a jour remplit la table mission_carte_destination

drop view if exists mission_continent_destination;
CREATE view mission_continent_destination AS
	select annee, sum(count) as count, sum(total_emission_co2) as total_emission_co2, continent 
	from mission_carte_destination 
	group by annee, continent 
	order by annee, total_emission_co2 desc;

