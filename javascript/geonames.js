/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */
 
 var rad = function(x) {
  return x * Math.PI / 180;
};


var geoname_user = 'intranetlesia';
var openrouteservice_token = "5b3ce3597851110001cf62481d956946b5b04ef895501f70a00b9efb";

// Calcule la géodésique entre 2 points du globe
function getDistance(p1lat, p1lng, p2lat, p2lng) {
	console.log(p1lat);
	console.log(p1lng);
	console.log(p2lat);
	console.log(p2lng);
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = rad(p2lat - p1lat);
  var dLong = rad(p2lng - p1lng);
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(p1lat)) * Math.cos(rad(p2lat)) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return Math.round(d/1000.0); // returns the distance in kilometer
}

// Affiche une distance en km avec 2 décimales
function distance_km(dist) {
  return Math.round(dist);

	// Pas de sprintf en JS !!!
	// console.log(dist);
	dist_km = Math.floor(dist);
	dist_hm = Math.floor((dist - dist_km)*10);
	dist_dm = Math.round((dist - dist_km - dist_hm/10)*100);
	if (dist_dm == 10) {
		dist_hm++;
		dist_dm = 0;
	}
	return  dist_km.toString() + "." + dist_hm.toString() + dist_dm.toString();
}

// Affiche une le poids en kg avec 3 décimales
function poids_kg(pds) {
	// Pas de sprintf en JS !!!
	// console.log("pds_kg");
	// console.log(pds);

  // 3 décimales
	pds_kg = Math.floor(pds);
	pds_hg = Math.floor((pds - pds_kg)*10);
	pds_dg = Math.floor((pds - pds_kg - pds_hg/10)*100);
	pds_g = Math.round((pds - pds_kg - pds_hg/10 - pds_dg/100)*1000);
	if (pds_g == 10) {
		pds_dg++;
		pds_g = 0;
	}
	// return  pds_kg.toString() + "." + pds_hg.toString() + pds_dg.toString() + pds_g.toString();

  // 2 décimale
	pds_kg = Math.floor(pds);
	pds_hg = Math.round((pds - pds_kg)*10);
	if (pds_hg == 10) {
		pds_kg++;
		pds_hg = 0;
	}
	return  pds_kg.toString() + "." + pds_hg.toString() ;
}

// Calcul du taux de CO2 émis en fonction des renseignements donnés
// Distance en km
// Mode = avion - tgvfrance - tgvinternational - autretrain - location - administratif - personnel - taxi - car - bus
function tauxco2(distance, distance_route, mode) {
	// console.log(distance);
	// console.log(distance_route);
	// console.log(mode);

	// prendre la distance retenue 
	distance = distance_retenue(distance, distance_route, mode);

	taux = 0;
	switch(mode) {
	  case 'avion':
	    if (distance < 500) {
	    	taux = 0.138 * distance;
	    } else if (distance < 1000) {
	    	taux = 69 + 0.101 * (distance - 500);
	    } else if (distance < 2000) {
	    	taux = 119.5 + 0.0825 * (distance - 1000);
	    } else if (distance < 5000) {
	    	taux = 202 + 0.087 * (distance - 2000);
	    } else {
	    	taux = 463 + 0.084 * (distance - 5000);
	    }
	    break;
	  case 'tgvfrance':
	    taux = 0.0034 * distance_route;
	    break;
	  case 'tgvinternational':
	    taux = 0.016  * distance_route;
	    break;
	  case 'trainfrance':
	    taux = 0.016  * distance_route;
	    break;
	  case 'traininternational':
	    taux = 0.035  * distance_route;
	    break;
	  case 'vlocation':
	  case 'vadministratif':
	  case 'vpersonnel':
	    taux = 0.170 * distance_route;
	    break;
	  case 'taxi':
	    taux = 0.470 * distance_route;
	    break;
	  case 'car':
	    taux = 0.180 * distance_route;
	    break;
	  case 'bus':
	    taux = 0.100 * distance_route;
	    break;
	  case 'covoiturage':
	    taux = 0;
	    break;
	  default:
	} 
	return taux;
}

// Renvoie la distance retenue en fonction du mode
function distance_retenue(distance, distance_route, mode) {
  var d_ret = distance_route;
	switch(mode.toLowerCase()) {
	  case 'avion':
      d_ret = distance;
	    break;
	  case 'tgvfrance':
	  case 'tgvinternational':
	  case 'trainfrance':
	  case 'traininternational':
	  case 'vlocation':
	  case 'vadministratif':
	  case 'vpersonnel':
	  case 'covoiturage':
	  case 'taxi':
	  case 'car':
	  case 'bus':
	  	 if ((distance_route == 0) || (distance_route == "")) {
	  	 	d_ret = distance;
	  	 } else {
     	 	d_ret = distance_route;
     	 }
	    break;
	  default:
	} 
	return d_ret;
}

// callback appelée quand on change le menu "Mode de transport"
jQuery.fn.update_coutco2 = function() {
	console.log("update_coutco2()");

	// on update le coût CO2 de l'étape en question
	var mode = this.parentsUntil("fieldset").parent().find(".mode_transport");
	var coutco2 = this.parentsUntil("fieldset").parent().find(".coutco2");
	var d_ret = this.parentsUntil("fieldset").parent().find(".geodistance_retenue");

	if (mode.val() != "") {
		var cities = this.parentsUntil("fieldset").parent().find(".geocity.ui-autocomplete-selected");

		if (cities.length == 2) {
			var distance = this.parentsUntil("fieldset").parent().find(".geodistance")[0].value;
			var distance_route = this.parentsUntil("fieldset").parent().find(".geodistance_route")[0].value;

			coutco2.val(poids_kg(tauxco2(distance, distance_route, mode.val())));
			coutco2.addClass("ui-autocomplete-selected").removeClass("erreur");

     	d_ret.val(distance_retenue(distance, distance_route, mode.val()));
			d_ret.addClass("ui-autocomplete-selected").removeClass("erreur");

		} else {
		  coutco2[0].value = "";
	      d_ret[0].value = "" ;
	    }
	} else {
		coutco2[0].value = "";
    	d_ret[0].value = "" ;
	}

  if (d_ret[0].value == "") {
		d_ret.removeClass("ui-autocomplete-selected");
  }

  if (coutco2[0].value == "") {
		coutco2.removeClass("ui-autocomplete-selected");
  }

	update_total_co2();
} ;

function update_total_co2() {
	// console.log("update_total_co2()");

  if ($(".coutco2").length == 0) { return ; }

	// console.log("total_co2");
	total_co2 = 0;
	// console.log(total_co2);
	$(".coutco2").each(function(){
		if ($(this).val()) {
			total_co2 += parseFloat($(this).val());
			// console.log(parseFloat($(this).val()));
		}
	});
	if (total_co2 > 0) {
		$(".total_coutco2")[0].value = poids_kg(total_co2);
		$(".total_coutco2").addClass("ui-autocomplete-selected").removeClass("erreur");
	} else {
    $(".total_coutco2")[0].value = 0;  
		$(".total_coutco2").removeClass("ui-autocomplete-selected").removeClass("erreur");
  }
	// console.log(total_co2);
}

// Appliquée sur un .geocity retire la surbrillance de la ville et des distances calculées
jQuery.fn.remove_highlight = function() {
	$(this).removeClass("ui-autocomplete-selected").removeClass("erreur");
	if ($(this).hasClass("geocity_depart")) {
		$(this).parentsUntil("fieldset").parent().find(".geocity_depart_lat").removeClass("ui-autocomplete-selected");
		$(this).parentsUntil("fieldset").parent().find(".geocity_depart_lon").removeClass("ui-autocomplete-selected");
	}
	if ($(this).hasClass("geocity_arrivee")) {
		$(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lat").removeClass("ui-autocomplete-selected");
		$(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lon").removeClass("ui-autocomplete-selected");
	}
	$(this).parentsUntil("fieldset").parent().find(".geodistance").removeClass("ui-autocomplete-selected");
	$(this).parentsUntil("fieldset").parent().find(".geodistance_route").removeClass("ui-autocomplete-selected");
	$(this).parentsUntil("fieldset").parent().find(".geodistance_retenue").removeClass("ui-autocomplete-selected");
	$(this).parentsUntil("fieldset").parent().find(".coutco2").removeClass("ui-autocomplete-selected");
};

// Appliquée sur un .geocity remets à zéro les distances calculées
jQuery.fn.reset_calculs = function() {
	if ($(this).hasClass("geocity_depart")) {
		$(this).parentsUntil("fieldset").parent().find(".geocity_depart_lat")[0].value = "";
		$(this).parentsUntil("fieldset").parent().find(".geocity_depart_lon")[0].value = "";
	}
	if ($(this).hasClass("geocity_arrivee")) {
		$(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lat")[0].value = "";
		$(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lon")[0].value = "";
	}
	$(this).parentsUntil("fieldset").parent().find(".geodistance")[0].value = "";
	$(this).parentsUntil("fieldset").parent().find(".geodistance_route")[0].value = "";
	$(this).parentsUntil("fieldset").parent().find(".geodistance_retenue")[0].value = "";
	$(this).parentsUntil("fieldset").parent().find(".coutco2")[0].value = "";
	update_total_co2();
};


// Recherche du code IATA depuis une réponse de geoname
function search_iata_code(reponse) {
	for (var i = 0; i < reponse.alternateNames.length - 1; i++) {
		if (reponse.alternateNames[i].lang == "iata") {
			return reponse.alternateNames[i].name;
		}
	}
	return "";
}

// supprime les doublons dans les resultats de recherche
function rep_unique(tab) {
	for (var i = tab.length - 1; i > 0; i--) {
		for (var j = i - 1; j >= 0; j--) {
			if ((tab[i].value == tab[j].value)
				&& (tab[i].label == tab[j].label)
				&& (tab[i].latitude == tab[j].latitude)
				&& (tab[i].longitude == tab[j].longitude)) {
				tab.splice(i, 1);
				break;
			}
		}
	}
	return tab;
}


function update_champ_debut() {
  	// on copie les valeurs présentes
  	console.log("update_champ_debut()");
  	$("#champ_date_5_date").val($("#champ_date_3_date").val());
  	$("#champ_date_5_heure").val($("#champ_date_3_heure").val());
}


// ajoute un event listener sur le champ date de la première étape
function set_change_champ_debut() {
	$("#champ_date_3_date").on("change", function(){
  		update_champ_debut();
  	});
  	$("#champ_date_3_heure").on("change", function(){
  		update_champ_debut()
  		;
  	});

  	return ;
}


function update_champ_fin() {
  	console.log("update_champ_fin() " + $(".fieldset.etape:visible:last .date").attr("id"));
  	$("#champ_date_6_date").val($(".fieldset.etape:visible:last .date").val());
  	$("#champ_date_6_heure").val($(".fieldset.etape:visible:last .heure").val());
}

// ajoute un event listener sur le champ date de la dernière étape
function set_change_champ_fin() {
	console.log("set_change_champ_fin() " + $(".fieldset.etape:visible:last .date").attr("id"));

	// on commence par supprimer tous les listener présents
	$(".fieldset.etape .date").off("change");
	$(".fieldset.etape .heure").off("change");

	// on le remet sur la dernière étape
	$(".fieldset.etape:visible:last .date").on("change", function(){
  		update_champ_fin();
  	});

  	$(".fieldset.etape:visible:last .heure").on("change", function(){
  		update_champ_fin();
  	});

  	// et on remet la 1e étape, disparue avec la 1e instruction
  	set_change_champ_debut();

  	return ;
}


// Autocompletion du nom des villes 
jQuery.fn.geonames_autocomplete = function() {
	this.autocomplete({
		source: function(request, response) {
			var reponses_villes = new Array();
			var reponses_aeroports = new Array();
			var reponses_obs = new Array();
			var reponses_air2 = new Array();
			var reponses_air3 = new Array();
			var label_villes = [{value: " ", label: " -- Villes"}]
			var label_aeroports = [{value: " ", label: " -- Aéroports"}]
			var label_obs = [{value: " ", label: " -- Observatoires"}]
			$.ajax({
				url: "https://secure.geonames.org/searchJSON",
				dataType: "json",
				data: {
					featureClass: "P",
					style: "medium",
					lang: "fr",
					maxRows: 12,
					name_startsWith: request.term,
					username: geoname_user
				},
				success: function(data) {
					rep1 = $.map(data.geonames, function( item ) {
						return {
							value: item.name + ", " + item.countryName ,
							label: item.name + ", " + item.adminName1 + ", " + item.countryName ,
							latitude: item.lat,
							longitude: item.lng,
						}
					});
					reponses_villes = [].concat(label_villes, rep1);
					response([].concat(reponses_villes, reponses_aeroports, reponses_obs));
					// console.log(reponses_villes);
				}
			});
			$.ajax({
				url: "https://secure.geonames.org/searchJSON",
				dataType: "json",
				data: {
					featureClass: "S",
					featureCode: "AIRP",
					style: "medium",
					lang: "fr",
					maxRows: 12,
					name_equals: request.term,
					searchlang: 'iata',
					username: geoname_user
				},
				success: function(data) {
					reponses_air2 = $.map(data.geonames, function( item ) {
						return {
							value: item.name + " (" + request.term.toUpperCase() + "), " + item.countryName ,
							label: item.name + " (" + request.term.toUpperCase() + "), " + item.countryName ,
							latitude: item.lat,
							longitude: item.lng,
						}
					});
					if ((reponses_air2.length + reponses_air3.length) > 0) {
						reponses_aeroports = rep_unique([].concat(label_aeroports, reponses_air2, reponses_air3));
					} else {
						reponses_aeroports = reponses_air2;
					}
					response([].concat(reponses_villes, reponses_aeroports, reponses_obs));
					// console.log(reponses_aeroports);
				}
			});
			$.ajax({
				url: "https://secure.geonames.org/searchJSON",
				dataType: "json",
				data: {
					featureClass: "S",
					featureCode: "AIRP",
					style: "full",
					lang: "fr",
					maxRows: 12,
					name: request.term,
					username: geoname_user
				},
				success: function(data) {
					reponses_air3 = $.map(data.geonames, function( item ) {
						var code = search_iata_code(item).toUpperCase();
						var aff_value = " (" + code + "), ";
						var aff_label = " (" + code + "), ";
						if (code == "") {
							aff_label = ", " + item.adminName1 + ", ";
							aff_value = ", ";
						}
						return {
							value: item.name + aff_value + item.countryName ,
							label: item.name + aff_label + item.countryName ,
							latitude: item.lat,
							longitude: item.lng,
						}
					});
					if ((reponses_air2.length + reponses_air3.length) > 0) {
						reponses_aeroports = rep_unique([].concat(label_aeroports, reponses_air2, reponses_air3));
					} else {
						reponses_aeroports = reponses_air3;
					}
					response([].concat(reponses_villes, reponses_aeroports, reponses_obs));
					// console.log(reponses_aeroports);
				}
			});
			$.ajax({
				url: "https://secure.geonames.org/searchJSON",
				dataType: "json",
				data: {
					featureClass: "S",
					featureCode: "OBS",
					style: "medium",
					lang: "fr",
					maxRows: 12,
					name: request.term,
					username: geoname_user
				},
				success: function(data) {
					rep2 = $.map(data.geonames, function( item ) {
						return {
							value: item.name + ", " + item.countryName ,
							label: item.name + ", " + item.adminName1 + ", " + item.countryName ,
							latitude: item.lat,
							longitude: item.lng,
						}
					});
					if (rep2.length > 0) {
						reponses_obs = [].concat(label_obs, rep2);
					}
					response([].concat(reponses_villes, reponses_aeroports, reponses_obs));
					// console.log(reponses_villes);
				}
			});
		},
		minLength: 2,
		search: function( event, ui ) {
			$(this).remove_highlight();
			$(this).reset_calculs();
		},
		select: function( event, ui ) {
			if (ui.item.value.trim() != "") {
			// console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
			$(this).addClass("ui-autocomplete-selected");
			
			// on trouve les champs lat/lon associés à cette étape
			var p1lat = $(this).parentsUntil("fieldset").parent().find(".geocity_depart_lat");
			var p1lng = $(this).parentsUntil("fieldset").parent().find(".geocity_depart_lon");
			var p2lat = $(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lat");
			var p2lng = $(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lon");

			// on remplit les coordonnées au bon endroit
			if ($(this).hasClass("geocity_depart")) {
				// console.log("geocity_depart");
				p1lat[0].value = ui.item.latitude;
				p1lng[0].value = ui.item.longitude;
				p1lat.addClass("ui-autocomplete-selected");
				p1lng.addClass("ui-autocomplete-selected");
			}
			if ($(this).hasClass("geocity_arrivee")) {
				// console.log("geocity_arrivee");
				p2lat[0].value = ui.item.latitude;
				p2lng[0].value = ui.item.longitude;
				p2lat.addClass("ui-autocomplete-selected");
				p2lng.addClass("ui-autocomplete-selected");
			}
			
			// on récupère les éléments de distance à mettre à jour
			var cities = $(this).parentsUntil("fieldset").parent().find(".geocity.ui-autocomplete-selected");
			var geodistance = $(this).parentsUntil("fieldset").parent().find(".geodistance");
			var geodistance_route = $(this).parentsUntil("fieldset").parent().find(".geodistance_route");
			var mode_transport = $(this).parentsUntil("fieldset").parent().find(".mode_transport");

			// si c'est la ville d'arrivée, on remplit l'étape de départ suivante
			if ($(this).hasClass("geocity_arrivee")) {
				var next_etape = $(this).parentsUntil("div.fieldset").parent().next(":visible");
				next_etape.find(".geocity_depart").val(ui.item.value).addClass("ui-autocomplete-selected");
				next_etape.find(".geocity_depart_lat").val(ui.item.latitude).addClass("ui-autocomplete-selected");
				next_etape.find(".geocity_depart_lon").val(ui.item.longitude).addClass("ui-autocomplete-selected");
			}

			if (cities.length == 2) {

				// calcul de la distance à vol d'oiseau
				dist = getDistance(p1lat[0].value, p1lng[0].value, p2lat[0].value, p2lng[0].value);
				geodistance[0].value = distance_km(dist);
				geodistance.addClass("ui-autocomplete-selected");

				// calcul de la distance par la route
				$.ajax({
					url: "https://api.openrouteservice.org/v2/directions/driving-car",
					dataType: "json",
					data: {
						api_key: openrouteservice_token,
						start: p1lng[0].value.toString() + "," + p1lat[0].value.toString(),
						end: p2lng[0].value.toString() + "," + p2lat[0].value.toString()
					},
					success: function(data) {
						geodistance_route[0].value = distance_km(data.features[0].properties.summary.distance/1000.0) ;
						geodistance_route.addClass("ui-autocomplete-selected");

    				// on met ensuite à jour le coût co2
	    			mode_transport.update_coutco2();;
					}
				});

			}
			}
		},
		close: function( event, ui ) {
			// on récupère les éléments de distance à mettre à jour
			var cities = $(this).parentsUntil("fieldset").parent().find(".geocity.ui-autocomplete-selected");
			var mode_transport = $(this).parentsUntil("fieldset").parent().find(".mode_transport");
			$(this).removeClass("ui-autocomplete-loading");


			if (cities.length == 2) {
				// Si le nom des 2 villes commencent par Aérooprt, on choisir l'avion
				var patt_airp_fr = new RegExp("Aéroport");
				var patt_airp_en = new RegExp("Airport");
				if ((patt_airp_fr.test(cities[0].value) || patt_airp_en.test(cities[0].value)) && 
					(patt_airp_fr.test(cities[1].value) || patt_airp_en.test(cities[1].value))) {
					mode_transport.val("avion");
					mode_transport.update_coutco2();;
				}
			}
		}
	
	});	
} ;

$( document ).ready(function() {

	/* 
	 * Initialisation au chargement 
	 */

	$(".geocity").geonames_autocomplete();	

  	$(".saisie_geodistance").hide();

  	var nb_etapes = Math.floor($("#champ_selection_1").val());

	// si une ville a sa latitude et longitude de renseigné, elle passe en ui-autocomplete-selected
	$(".geocity").each(function(){
		if ($(this).val() && $(this).hasClass("geocity_depart")) {
			if ($(this).parentsUntil("fieldset").parent().find(".geocity_depart_lat")[0].value &&
				$(this).parentsUntil("fieldset").parent().find(".geocity_depart_lon")[0].value) {
				$(this).addClass("ui-autocomplete-selected");
			}
		}
		if ($(this).val() && $(this).hasClass("geocity_arrivee")) {
			if ($(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lat")[0].value &&
				$(this).parentsUntil("fieldset").parent().find(".geocity_arrivee_lon")[0].value) {
				$(this).addClass("ui-autocomplete-selected");
			}
		}
		var cities = $(this).parentsUntil("fieldset").parent().find(".geocity.ui-autocomplete-selected");
		var mode = $(this).parentsUntil("fieldset").parent().find(".mode_transport");

		if (cities.length == 2) {
			if ($(this).parentsUntil("fieldset").parent().find(".geodistance")[0].value) {
				$(this).parentsUntil("fieldset").parent().find(".geodistance").addClass("ui-autocomplete-selected").removeClass("erreur");
			}
			if ($(this).parentsUntil("fieldset").parent().find(".geodistance_route")[0].value) {
				$(this).parentsUntil("fieldset").parent().find(".geodistance_route").addClass("ui-autocomplete-selected").removeClass("erreur");
			}
			if ($(this).parentsUntil("fieldset").parent().find(".geodistance_retenue")[0].value) {
				$(this).parentsUntil("fieldset").parent().find(".geodistance_retenue").addClass("ui-autocomplete-selected").removeClass("erreur");
			}
			if ($(this).parentsUntil("fieldset").parent().find(".coutco2")[0].value && (mode.val() != "")) {
				$(this).parentsUntil("fieldset").parent().find(".coutco2").addClass("ui-autocomplete-selected").removeClass("erreur");
			}
		}

	});

	update_total_co2();

  	/*
  	 * Met à jour automatiquement les horaires de début et de fin en fonction des étapes
  	 */

  	 set_change_champ_debut();
  	 set_change_champ_fin();


	/* 
	 * Traitement des actions 
	 */

	 // quand on sélectionne un demandeur, les champs noms et emails sont renseignés
	 // seulement si ces champs sont vides
	 $("#champ_auteurs_1").on("change", function(){
	 	var id_auteur = $(this).val();
	 	var nom = $("#get_emails option[value="+id_auteur+"]").html();
	 	var email = $("#get_emails option[value="+id_auteur+"]").attr("data-email").toLowerCase();
	 	if (($("#champ_input_4").val() == '') && ($("#champ_input_45").val() == '')) {
		 	$("#champ_input_4").val(nom);
		 	$("#champ_input_45").val(email);
	 	} else {
	 		msg  = 'Voulez-vous remplacer les valeurs actuelles du missionnaire ';
	 		msg += 'par le nom et email de ' + nom + '?';
	 		if (confirm(msg)) {
			 	$("#champ_input_4").val(nom);
			 	$("#champ_input_45").val(email);	
	 		}
	 	}
	 });

	 // si on supprime la valeur d'une ville, on la remet en blanc et on remet les calcul à 0
	$(".geocity").on("change", function(){
		if ($(this).val() == "") {
			$(this).remove_highlight();
			$(this).reset_calculs();
		}
	});

	$(".mode_transport").on("change", function() { 
    // on recalcule les emissions co2
		$(this).update_coutco2(); 
		if ($(this).val != "") $(this).removeClass("erreur");

    // on vide l'immatriculation
    // $(this).parentsUntil("fieldset").parent().find(".immatriculation")[0].value = "";
    // $(this).parentsUntil("fieldset").parent().find(".vehicule_administratif")[0].value = "";

	});

	// remet à 0 une étape
	function reset_etape(i) {
		$(".etape_"+i+" .geocity").val("").removeClass("ui-autocomplete-selected");
		$(".etape_"+i+" .geodistance").val("").removeClass("ui-autocomplete-selected");
		$(".etape_"+i+" .geodistance_retenue").val("").removeClass("ui-autocomplete-selected");

		$(".etape_"+i+" .geocity_depart_lat").val("").removeClass("ui-autocomplete-selected");
		$(".etape_"+i+" .geocity_arrivee_lat").val("").removeClass("ui-autocomplete-selected");
		$(".etape_"+i+" .geocity_depart_lon").val("").removeClass("ui-autocomplete-selected");
		$(".etape_"+i+" .geocity_arrivee_lon").val("").removeClass("ui-autocomplete-selected");

		$(".etape_"+i+" .coutco2").val(0).removeClass("ui-autocomplete-selected");

		$(".etape_"+i+" .date").val("");
		$(".etape_"+i+" .heure").val("");

		$(".etape_"+i+" .mode_transport").val("");
	}

	// quand on change le nb d'étapes, 
	// on efface les distances calculées pour les étapes cachées
	// et on recalcule les étapes affichées

	$("#champ_selection_1").on("change", function(){
		var nb_etapes = Math.floor($("#champ_selection_1").val());
		// console.log(nb_etapes);

		for (i=1; i<=nb_etapes; i++) 
			$(".etape_"+i+" .mode_transport").change();

		for (i=(nb_etapes+1); i<=10; i++)
			reset_etape(i);

		update_total_co2();
		update_champ_debut();
		update_champ_fin();
	});

	// quand un bloc "étape" reçoit le focus, on remet les listeners comme il faut
	$(".fieldset.etape input.date").on("focus", function(){
		$(this).removeClass("erreur");
		set_change_champ_fin();
		update_champ_debut();
		update_champ_fin();
	});
	$(".fieldset.etape input.heure").on("focus", function(){
		$(this).removeClass("erreur");
	});


	// remplit l'immatriculation quand on sélectionne un véhicule administratif
  $(".vehicule_administratif").on("change", function() {

    var vehicule = $(this).val();
    var immat = "";

    switch (vehicule) {
      case "partner":
        immat = "DE-640-TS";
        break;
      case "kangoo":
        immat = "75N 509 7F";
        break;
      case "auris":
        immat = "EG-048-DV";
        break;
    }

    $(this).parentsUntil("fieldset").parent().find(".immatriculation")[0].value = immat;

  });

  	/*
  	 * fabrique l'itinéraire retour symétrique de celui de l'aller
  	 */

  	$(".aller_retour button.remplir_retour").on("click", function(){
		var nb_etapes = Math.floor($("#champ_selection_1").val());
		// console.log("nb_etapes : " + nb_etapes);
		if (nb_etapes%2 !== 0) {
			alert("Le nombre d'étapes doit être pair pour utiliser cette fonction");
			return false;
		}

		// il faut que le nombre d'étapes à remplir soit >= à celui rempli
		var nb_remplies = $(".coutco2.ui-autocomplete-selected").length;
		// console.log("nb_remplies : " + nb_remplies);
		if (nb_remplies > (nb_etapes/2)) {
			alert("Il y a trop d'étapes déjà remplies");
			return false;
		} 

		for (i=1; i<=nb_etapes/2; i++) {
			j = nb_etapes - i + 1; // numéro de l'étape symétrique

			// on ne fait le remplissage que si les villes sont remplies
			if (($(".etape_"+i+" input.geocity_depart.ui-autocomplete-selected").length > 0) 
				&& ($(".etape_"+i+" input.geocity_arrivee.ui-autocomplete-selected").length > 0)) {


				$(".etape_"+j+" input.geocity_arrivee").val($(".etape_"+i+" input.geocity_depart").val());
				$(".etape_"+j+" input.geocity_arrivee_lat").val($(".etape_"+i+" input.geocity_depart_lat").val());
				$(".etape_"+j+" input.geocity_arrivee_lon").val($(".etape_"+i+" input.geocity_depart_lon").val());

				$(".etape_"+j+" input.geocity_depart").val($(".etape_"+i+" input.geocity_arrivee").val());
				$(".etape_"+j+" input.geocity_depart_lat").val($(".etape_"+i+" input.geocity_arrivee_lat").val());
				$(".etape_"+j+" input.geocity_depart_lon").val($(".etape_"+i+" input.geocity_arrivee_lon").val());

				$(".etape_"+j+" input.geodistance").val($(".etape_"+i+" input.geodistance").val());
				$(".etape_"+j+" input.geodistance_route").val($(".etape_"+i+" input.geodistance_route").val());

				$(".etape_"+j+" input.geodistance_retenue").val($(".etape_"+i+" input.geodistance_retenue").val());
				$(".etape_"+j+" input.coutco2").val($(".etape_"+i+" input.coutco2").val());

				$(".etape_"+j+" input.geocity_depart").addClass("ui-autocomplete-selected");
				$(".etape_"+j+" input.geocity_arrivee").addClass("ui-autocomplete-selected");

				$(".etape_"+j+" input.geodistance_retenue").addClass("ui-autocomplete-selected");
				$(".etape_"+j+" input.coutco2").addClass("ui-autocomplete-selected");

				$(".etape_"+j+" .mode_transport").val($(".etape_"+i+" .mode_transport").val());
				$(".etape_"+j+" .mode_transport").update_coutco2();

			}
		}
		
		target_obj = $(".etape_"+(nb_etapes/2+1));
		$('html, body').animate({
            scrollTop: target_obj.offset().top
        }, 500);


		return false;
  	});


  	/*
  	 * efface l'itinéraire retour 
  	 */


  	$(".aller_retour button.effacer_retour").on("click", function(){
		var nb_etapes = Math.floor($("#champ_selection_1").val());
		// console.log("nb_etapes : " + nb_etapes);
		if (nb_etapes%2 !== 0) {
			alert("Le nombre d'étapes doit être pair pour utiliser cette fonction");
			return false;
		}

		if (nb_etapes == 2) {
			msg = "Effacer l'étape 2 ?";
		} else {
			msg = "Effacer les étapes " + (nb_etapes/2+1) + " à " + nb_etapes + "?";
		}

		if (confirm(msg)) {
			for (i=nb_etapes/2+1; i<=nb_etapes; i++) {
				reset_etape(i);
			}
			target_obj = $(".etape_"+(nb_etapes/2+1));
			$('html, body').animate({
                scrollTop: target_obj.offset().top
            }, 500);
		}

		return false;
	});

  	/* 
  	 * Quand on modifie la case "Le demandeur est-il le missionnaire"
  	 * on efface la valeur dans nom et email du missionnaire + statut
  	 */

  	 var nom_missionnaire = $("#champ_input_4").attr("value");
  	 var email_missionnaire = $("#champ_input_45").attr("value");
  	 var statut = $("#champ_selection_22 option[selected=selected]").val();

  	$(".form_demande_mission .editer_oui_non_3 input").on("click", function() {
	  	
	  	// oui est coché, on remet les valeurs du chargement
	  	// et on replie le cadre 'details'
	  	$(".form_demande_mission #champ_oui_non_3_oui:checked").each(function() {
	  		$("#champ_input_4").val(nom_missionnaire);
	  		$("#champ_input_45").val(email_missionnaire );
	  		$("#champ_selection_22").val(statut );
	  		$(".fieldset_fieldset_26").removeClass("plie");
	  		$(".fieldset_fieldset_26 .editer-groupe").slideUp();
	  	});

	  	// non est coché, on efface les valeurs du chargement
	  	// et on déplie le bloc details
	  	$(".form_demande_mission #champ_oui_non_3_non:checked").each(function() {
	  		// on stocke les valeurs (elles ont pu etre modifiées)
		  	nom_missionnaire = $("#champ_input_4").val();
		  	email_missionnaire = $("#champ_input_45").val();
		  	statut = $("#champ_selection_22").val();
	  		$("#champ_input_4").val("");
	  		$("#champ_input_45").val("");
	  		$("#champ_selection_22").val("invite");
	  		$(".fieldset_fieldset_26").addClass("plie");
	  		$(".fieldset_fieldset_26 .editer-groupe").slideDown();
	  	});
	});

  /* 
   * Validation du formulaires
   */

	// quand on soumet le formulaire, on vérifie que les étapes sont correctement remplies en fonction du champ 
	// @selection_1@ : Nombre d'étapes 
	$(".form_demande_mission form").on("submit", function() {
		var nb_etapes =  Math.floor($("#champ_selection_1:visible").val());
		nb_etapes = isNaN(nb_etapes) ? 0 : nb_etapes;
		console.log("nb_etapes = " +nb_etapes);
		var err_i = 0;
		var err_s = 0;
		var err_d = 0;
		var err_h = 0;
		var err_r = 0;
		var target_obj = null;
		var debut_mission = null;
		var fin_mission = null;
		var date_etape_1 = null;
		var date_etape_fin = null;
		var target = "";
   
	    // au cas où il y aurait eu un bug JS, on recalcule le coût co2
	    $("select.mode_transport").each(function() {
	      $(this).update_coutco2();
	    });
	    update_total_co2();

		// console.log("form_demande_mission form.submit");
		// console.log(nb_etapes);

		for (i=1; i<=nb_etapes; i++) {
			// console.log("i="+i);
			$(".etape_"+i+" input.geocity").each(function(){
				// console.log("erreur "+i+" "+$(this).attr("id"));
				// console.log(($(this).val() == ""));
				// console.log((! $(this).hasClass("ui-autocomplete-selected")));
				if (($(this).val() == "") || (! $(this).hasClass("ui-autocomplete-selected"))) {
					$(this).addClass("erreur");
					// on récupère le fieldset parent pour faire un scroll 
					// on scroll jusqu'à la 1e étape en erreur
					// on suppose que la personne verra les champs en rouge dessous
					if (target_obj == null) target_obj = $(this).parentsUntil("fieldset").parent();
					err_i++;
				} 
			});
			$(".etape_"+i+" select.mode_transport").each(function(){
				// console.log("erreur "+i+" "+$(this).attr("id"));
				// console.log(($(this).val() == ""));
				if ($(this).val() == "") {
					$(this).addClass("erreur");
					if (target_obj == null) target_obj = $(this).parentsUntil("fieldset").parent();
					err_s++;
				}
			});
			$(".etape_"+i+" input.date").each(function(){
				// console.log("erreur "+i+" "+$(this).attr("id"));
				// console.log(($(this).val() == ""));
				if ($(this).val() == "") {
					$(this).addClass("erreur");
					if (target_obj == null) target_obj = $(this).parentsUntil("fieldset").parent();
					err_d++;
				}
			});
			$(".etape_"+i+" input.heure").each(function(){
				// console.log("erreur "+i+" "+$(this).attr("id"));
				// console.log(($(this).val() == ""));
				if ($(this).val() == "") {
					$(this).addClass("erreur");
					if (target_obj == null) target_obj = $(this).parentsUntil("fieldset").parent();
					err_h++;
				}
			});
			$(".etape_"+i+" input.geodistance_retenue").each(function(){
				// console.log("erreur "+i+" "+$(this).attr("id"));
				// console.log(($(this).val() == ""));
				if ($(this).val() == "") {
					$(this).addClass("erreur");
					if (target_obj == null) target_obj = $(this).parentsUntil("fieldset").parent();
					err_r++;
				}
			});
		}

    if (err_r > 0) {
			alert("Une étape contient un trajet non valide ou impossible");		
            $('html, body').animate({
                scrollTop: target_obj.offset().top
            }, 500);
			return false;
    }

		if ((err_i + err_s + err_d + err_h) > 0) {
			if (err_i > 0) {
				target = "les villes de départ et d'arrivée";
			}
			if (err_s > 0) {
				if (target != "") target += " et ";
				target += "le mode de transport";
			}
			if ((err_d > 0)||(err_h > 0)) {
				if (target != "") target += " et ";
				target += "les horaires";
			}
			alert("Vous devez saisir "+target+" pour les "+nb_etapes+ " étapes");		
            $('html, body').animate({
                scrollTop: target_obj.offset().top
            }, 500);
			return false;
		}

		// on vérifie que le début de la mission est avant la fin
		$("input#champ_date_5_date").each(function(){
		 	debut_mission = $(this).val().split("/").reverse().join("-");
		});
		$("input#champ_date_5_heure").each(function(){
		 	debut_mission = debut_mission + " " + $(this).val();
		});

		$("input#champ_date_6_date").each(function(){
		 	fin_mission = $(this).val().split("/").reverse().join("-");
		});
		$("input#champ_date_6_heure").each(function(){
		 	fin_mission = fin_mission + " " + $(this).val();
		});

		debut_mission = new Date(debut_mission);
		fin_mission = new Date(fin_mission);
		target_obj = null

		if (fin_mission < debut_mission) {
			if (target != "") target += "\n";
			target += "La date de fin de mission est avant celle du début";
			target_obj = $("input#champ_date_6_date").parentsUntil("fieldset").parent();
			$("input#champ_date_6_date").addClass("erreur");
			$("input#champ_date_6_heure").addClass("erreur");
		}

		if (nb_etapes > 0) {
			// on vérifie que les horaires de début et de fin sont compatibles avec les étapes

			$(".etape_1 input.date").each(function(){
			 	date_etape_1 = $(this).val().split("/").reverse().join("-");
			});
			$(".etape_1 input.heure").each(function(){
			 	date_etape_1 = date_etape_1 + " " + $(this).val();
			});

			$(".etape_"+nb_etapes+" input.date").each(function(){
			 	date_etape_fin = $(this).val().split("/").reverse().join("-");
			});
			$(".etape_"+nb_etapes+" input.heure").each(function(){
			 	date_etape_fin = date_etape_fin + " " + $(this).val();
			});

			date_etape_1 = new Date(date_etape_1);
			date_etape_fin = new Date(date_etape_fin);

			if (debut_mission > date_etape_1) {
				target = "La date de début de mission est après celle de la 1e étape."
				target_obj = $("input#champ_date_5_date").parentsUntil("fieldset").parent();
				 $("input#champ_date_5_date").addClass("erreur");
				 $("input#champ_date_5_heure").addClass("erreur");
			}

			if (fin_mission < date_etape_fin) {
				if (target != "") target += "\n";
				target += "La date de fin de mission est avant celle de la dernière étape";
				target_obj = $("input#champ_date_6_date").parentsUntil("fieldset").parent();
				$("input#champ_date_6_date").addClass("erreur");
				$("input#champ_date_6_heure").addClass("erreur");
			}

		}


		if (target_obj != null) {
			target += "\nConfirmez-vous ces dates ?";
			if (confirm(target)) {
				return true;
			} else {	
	            $('html, body').animate({
	                scrollTop: target_obj.offset().top
	            }, 500);
				return false;
			}
		}

		return true;
	});

});



