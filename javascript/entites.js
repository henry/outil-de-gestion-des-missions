/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */
 
  // affiche les conges d'un gestionnaire lorsqu'il est sélectionné
  function affiche_conges() {
    var id_auteur = $(".entite_mission #champ_destinataires_1").val();
    if (id_auteur != ''){
        if ($("#conges_gestionnaires option." + id_auteur).length > 0) {
          var msg = "Absent·e ";
          $("#conges_gestionnaires option." + id_auteur).each(function(){
            if ($(this).attr("data-debut-conge") == $(this).attr("data-fin-conge")) {
              msg += "le " + $(this).attr("data-debut-conge") + " ; ";
            } else {
              msg += "du " + $(this).attr("data-debut-conge") + " au " + $(this).attr("data-fin-conge") + " ; ";
            }
            $(".entite_mission .editer_destinataires_1 .explication").html(msg);
          });

          // un peu de highlight nécessaire pour attirer l'attention surtout si on passe d'un gestionnaire à l'autre
          var col = $(".entite_mission .editer_destinataires_1 .explication").css("background-color");
          $(".entite_mission .editer_destinataires_1 .explication").
            show(500).
            animate({"background-color": "yellow"}, 50).
            animate({"background-color": col}, 500);

        } else {
          $(".entite_mission .editer_destinataires_1 .explication").hide(500);
          $(".entite_mission .editer_destinataires_1 .explication").html('');
        }
      } else {
          $(".entite_mission .editer_destinataires_1 .explication").hide(500);
          $(".entite_mission .editer_destinataires_1 .explication").html('');
      }
  }

  // opérations à effectuer lorsq'une entité est déssaisie
  function reset_entite() {
      $(".entite_mission .choix_entite").val("");
      $(".entite_mission .choix_entite").removeClass("ui-autocomplete-selected");
      $(".entite_mission #champ_destinataires_1").removeClass("ui-autocomplete-selected");
      $(".entite_mission #champ_destinataires_1").val("");
      $(".entite_mission #champ_auteurs_2").removeClass("ui-autocomplete-selected");
      $(".entite_mission #champ_auteurs_2").val("");
      $(".entite_mission #champ_hidden_42").val("non");
      $(".entite_mission #champ_hidden_42").change();

  }

  // opérations à effectuer quand une entité est choisie
  // entite : string = nom de l'entité choisir
  // element : élément Jquery qui contient l'autocomplétion

  var entite_financiere = "";

  function choix_entite(entite, element) {
      element.addClass("ui-autocomplete-selected").removeClass("erreur");
      element.parent().removeClass("erreur");
      element.parent().find('.erreur_message').hide();

      var val_pat = entite.replace("&", "\\&").trim();

      var opt = $("#get_entites option[value=" + val_pat + "]");
      entite_financiere = entite;

      $(".entite_mission #champ_destinataires_1").val(opt.attr("data-gestionnaire"));
      $(".entite_mission #champ_destinataires_1").addClass("ui-autocomplete-selected").removeClass("erreur");
      $(".entite_mission #champ_destinataires_1").parent().removeClass("erreur");
      $(".entite_mission #champ_destinataires_1").parent().find('.erreur_message').hide();
      affiche_conges();

      $(".entite_mission #champ_auteurs_2").val(opt.attr("data-chef"));
      $(".entite_mission #champ_auteurs_2").addClass("ui-autocomplete-selected").removeClass("erreur");
      $(".entite_mission #champ_auteurs_2").parent().removeClass("erreur");
      $(".entite_mission #champ_auteurs_2").parent().find('.erreur_message').hide();

      $(".entite_mission #champ_auteurs_2 option").attr("disabled", "disabled");
      $(".entite_mission #champ_auteurs_2 option:selected").removeAttr("disabled");

      if (opt.attr("data-justificatif") == "oui") {
        $(".entite_mission #champ_hidden_42").val("oui");
        // nécessaire pour déclencher le mécanisme d'affichage de demande de justificatif
        $(".entite_mission #champ_hidden_42").change();
      } else {
        $(".entite_mission #champ_hidden_42").val("non");
        $(".entite_mission #champ_hidden_42").change();
      }
  }

$( function() {
  var availableEntites = [];
  $("#get_entites option").each( function( index){
    availableEntites.push( $(this).attr("value") );
  });

  
  $( ".entite_mission .choix_entite" ).autocomplete({
    source: availableEntites,
    minLength: 1,
    search: function( event, ui ) {
      $(this).removeClass("ui-autocomplete-selected");
      entite_financiere = "";

      // on retire la surbrillance des champs champs déduits
      $(".entite_mission #champ_destinataires_1").removeClass("ui-autocomplete-selected");
      $(".entite_mission #champ_auteurs_2").removeClass("ui-autocomplete-selected");

      // on remet leurs valeurs à 0 
      $(".entite_mission #champ_destinataires_1")[0].value = "";
      $(".entite_mission #champ_auteurs_2")[0].value = "";

      $(".entite_mission #champ_auteurs_2 option").removeAttr("disabled");
    },
    select: function( event, ui ) {
      choix_entite(ui.item.value, $(this));
    }
  });

  // on enlève la surbrillance quand on change les valeurs par défaut

  $(".entite_mission #champ_destinataires_1").on( "change", function() {
    $(this).removeClass("ui-autocomplete-selected");
    affiche_conges();
  });

  $(".entite_mission #champ_auteurs_2").on( "change", function() {
    $(this).removeClass("ui-autocomplete-selected");
  });

  // quand on quitte le champ, si une valeur n'a pas été choisie dans la liste
  // on la remet à 0 
  $( ".entite_mission .choix_entite" ).on("blur", function(){
    if (entite_financiere == "")  {
      reset_entite();
      $(this).addClass("erreur").removeClass("ui-autocomplete-selected");
    }
    if ($(this).val() == "" ) {
      entite_financiere = "";
      reset_entite();
    }
  });

  // si on supprime la valeur, sans rien taper, on remet tout à 0 aussi
  $( ".entite_mission .choix_entite" ).on("change", function(){
    if ($(this).val() == "" ) {
      reset_entite();
    }
  });

   // quand on choisit une mission sur des crédits non gérés au LESIA
   // cela sélectionne l'entité dépensière EXT

   $(".editer_radio_1 input").on("change", function(){
    if ($(this).val() == "exterieur") {
      $( ".entite_mission .choix_entite" ).each(function(){
          $("#champ_input_12")[0].value = "EXT";
          choix_entite("EXT", $(this));
      });
      
     } else {
        if ($("#champ_input_12").val() == "EXT") {
          reset_entite();
        }
     }
   });

  // au chargement de la page
  // si l'entité (input_12) est non nulle, on met en readonly le resp. scientifique
  if (($("input#champ_input_12").length > 0) && ($("input#champ_input_12")[0].value != "")) {
    $( ".entite_mission .choix_entite" ).addClass("ui-autocomplete-selected");
    $(".entite_mission #champ_destinataires_1").addClass("ui-autocomplete-selected");
    $(".entite_mission #champ_auteurs_2").addClass("ui-autocomplete-selected");

    $(".entite_mission #champ_auteurs_2 option").attr("disabled", "disabled");
    $(".entite_mission #champ_auteurs_2 option:selected").removeAttr("disabled");

  }



});
