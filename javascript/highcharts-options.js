/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */
 
 Highcharts.setOptions({
    // "#f7a35c", rouge-orange
    // "#90ed7d", vert
    // "#7cb5ec", bleu 
    // "#8085e9", violet
    // "#f15c80", rouge-rose
    // "#e4d354", jaune
    // "#2b908f", bleu-vert
    // "#f45b5b", rouge
    // "#91e8e1", cyan
    // "#434348"  noir
    colors: ["#f7a35c",  "#90ed7d", "#7cb5ec", 
    	"#8085e9", "#f15c80", "#e4d354", 
    	"#2b908f", "#f45b5b", "#91e8e1", "#434348"],
    lang: {
        months: [
            'Janvier', 'Février', 'Mars', 'Avril',
            'Mai', 'Juin', 'Juillet', 'Août',
            'Septembre', 'Octobre', 'Novembre', 'Décembre'
        ],
        shortMonths: ["Jan", "Fév", "Mars", "Avr", "Mai", "Juin", "Jul", "Août", "Sep", "Oct", "Nov", "Déc"],
        weekdays: [
            'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
            'Jeudi', 'Vendredi', 'Samedi'
        ]
    },   
    plotOptions: {
        series: {
            states: {
                inactive: {
                    opacity: 1 
                }
            }
        }
    },

    });