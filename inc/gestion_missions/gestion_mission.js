/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */
 
     $( function() {
      /* On rend modifiable les champs réservés aux gestionnaires */
      $(".reserve_gestionnaires input").removeAttr("readonly");
      $(".reserve_gestionnaires input").removeAttr("disabled");
      $(".reserve_gestionnaires input[type='hidden']").remove();
      
      $(".editer_auteurs_1 select").removeAttr("disabled");
      $(".editer_auteurs_1 input[type='hidden']").remove();

      /* On incrémente le champ nb_modifications */
      if ($("input[type='hidden'].nb_modifications").length > 0) {
        var nb_mod = Number($("input[type='hidden'].nb_modifications")[0].value);
        console.log("incrément " + nb_mod);
        $("input[type='hidden'].nb_modifications")[0].value = (nb_mod+1);
      }
    });

