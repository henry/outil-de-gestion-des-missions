<?php
/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */

if (!defined("_ECRIRE_INC_VERSION")) return;


function formulaires_maj_continent_mission_saisies(){
	return array();
}



function formulaires_maj_continent_mission_verifier(){
	return array();
}

function formulaires_maj_continent_mission_traiter(){

	$table = "mission_carte_destination";
	
	$geonames_params1 = array(
		type => "json",
		username => 'florence.henry'
	);
	$geonames_url1 = "https://secure.geonames.org/countryCode?";


	$geonames_params2 = array(
		featureClass => 'A',
		featureCode => 'PCLI',
		style => 'full',
		maxRows => 1,
		username => 'florence.henry'
	);
	// PHP ne gère pas quand on passe 2 fois le même paramètre avec des valeurs différentes
	$geonames_params2b = array(
		featureCode => 'PCLD',
	);
	$geonames_url2 = "https://secure.geonames.org/searchJSON?";

    if ($resultats = sql_select(array('latitude', 'longitude', 'destination'), $table, 'continent =\'\'') ) {
		while ($res = sql_fetch($resultats)) {

			$params = $geonames_params1;
			$params["lat"] = $res['latitude'];
			$params["lng"] = $res['longitude'];
			$url = $geonames_url1 . http_build_query($params) ;
			// error_log($url);

			$json = file_get_contents($url);
			$data1 = json_decode($json, true);
			// error_log("DATA 1");
			// error_log(print_r($data1, true));

			$params = $geonames_params2;
			$params["country"] = $data1["countryCode"];
			$url = $geonames_url2 . http_build_query($params) .'&'. http_build_query($geonames_params2b);
			// error_log($url);

			$json = file_get_contents($url);
			$data2 = json_decode($json, true);
			// error_log("DATA 2");
			// error_log(print_r($data2, true));

			foreach ($data2["geonames"] as $key => $val) {
				error_log($res["destination"] . ' - ' . $val['continentCode']);
				$set = array('continent' => $val['continentCode']);
				$where = "destination = ".sql_quote($res['destination']) ;

				$res = sql_updateq($table, $set, $where);
				// error_log(print_r($res, true));
			}

			$res = sql_updateq($table, $set, $where);

		}
    }
	
	$res = array("message_ok" => "Continents mis à jour");
	return $res;
}

