<?php
/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */

if (!defined("_ECRIRE_INC_VERSION")) return;


function formulaires_annuler_mission_saisies($id_reponse, $id_auteur ){
	$saisies = array(
		array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_reponse',
				'defaut' => $id_reponse,
				'obligatoire' => 'oui'
			)
		),
		array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_auteur',
				'defaut' => $id_auteur,
				'obligatoire' => 'oui'
			)
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'explication' => 'Entrez le motif de l\'annulation',
				'nom' => 'motif',
				'obligatoire' => 'oui'
			),
		),
	);
	
	return $saisies;
}



function formulaires_annuler_mission_verifier(){
	$erreurs = array();
	$message_erreur = array();

  /* On applique les vérifications de base */
  include_spip('inc/saisies');
  $saisies = formulaires_annuler_mission_saisies(
      _request('id_reponse'), 
      _request('id_auteur'), 
      _request('motif') );
  $erreurs = saisies_verifier($saisies); 
	
  /* Autres vérifications */

  /* id_reponse doit être une réponse du formulaire "Demande de mission" */
  /* id_auteur doit correspondre avec celui de la réponse */
  /* ou id_auteur dans le groupe "Administration"  ou "Gestionnaires" */


	$id_reponse = intval(_request('id_reponse'));
	$id_auteur = intval(_request('id_auteur'));

  $id_formulaire = sql_getfetsel("id_formulaire", "spip_formulaires", "identifiant = 'demande_mission'");
  $id_zone_gest  = sql_getfetsel("id_zone", "spip_zones", "titre = 'Gestionnaires'");
  $id_zone_admin = sql_getfetsel("id_zone", "spip_zones", "titre = 'Administration'");

  $where = array();
  $where[] = sql_in('id_zone', array($id_zone_admin, $id_zone_gest));
  $where[] = "objet = 'auteur'";
  $admins = sql_allfetsel("id_objet", "spip_zones_liens", $where);
  $ids_admin = array();
  foreach ($admins as $a) {
    $ids_admin[] = $a['id_objet'];
  }
  
	$where = array();
	$where[] = "id_formulaires_reponse = $id_reponse" ;
	$where[] = "id_formulaire = $id_formulaire" ;
	$where[] = "((id_auteur = $id_auteur) OR ($id_auteur IN (".implode(",", $ids_admin).")))" ;

	$res = sql_countsel("spip_formulaires_reponses", $where);
	if ($res != 1) {
		$erreurs['id_reponse'] = "Identifiant de réponse invalide.";
	}

	
	return $erreurs;
}

function formulaires_annuler_mission_traiter(){
	$id_reponse = intval(_request('id_reponse'));
	$id_auteur = intval(_request('id_auteur'));
	$motif = _request('motif');

  $table = "spip_formulaires_reponses_champs";
  $id_formulaire = sql_getfetsel("id_formulaire", "spip_formulaires", "identifiant = 'demande_mission'");

	$where = array();
	$where[] = "id_formulaires_reponse = $id_reponse" ;
	$where[] = "nom = 'input_44'" ;

  $count = sql_countsel($table, $where);
  /* nécessaire car le champ input_44 est masqué donc non posté par défaut */
  if ($count == 0) {

    $set = array(
      "id_formulaires_reponse" => $id_reponse,
      "nom" => 'input_44',
      "valeur" => $motif  
    );

    sql_insertq($table, $set);

  } else {

    $set = array("valeur" => $motif);

    sql_updateq($table, $set, $where);

  }

	$where = array();
	$where[] = "id_formulaires_reponse = $id_reponse" ;
	$where[] = "nom = 'oui_non_5'" ;
  $set = array("valeur" => "on");

  sql_updateq($table, $set, $where);

  /* On envoie un mail au gestionnaire */

	$where = array();
	$where[] = "id_formulaires_reponse = $id_reponse" ;
	$where[] = "nom = 'destinataires_1'" ;

	$gest = unserialize(sql_getfetsel("valeur", "spip_formulaires_reponses_champs", $where));
  $gest = $gest[0];

  $mail_gest = sql_getfetsel("email", "spip_auteurs","id_auteur = $gest");

//
//	$page = recuperer_fond("notifications/demande_confirmation_mission");
//	$page .= recuperer_fond("modeles/formulaires_reponse", array('id_formulaires_reponse' => $id_reponse));
//

    $page = recuperer_fond("notifications/annuler_mission", array(
      id_reponse => $id_reponse,
      nom_agent => utf8_decode($GLOBALS['visiteur_session']['nom']),
      motif => $motif));

	  $envoyer_mail = charger_fonction('envoyer_mail', 'inc');
	
  	$destinataire = $mail_gest;
  	$sujet = "Annulation de la mission pour " . utf8_decode($GLOBALS['visiteur_session']['nom']);
  	$corps = array( "html" => $page, "nom_envoyeur" => utf8_decode($GLOBALS['visiteur_session']['nom']) );
  	$from = $GLOBALS['visiteur_session']['email'];
	
  	$envoyer_mail($destinataire, $sujet, $corps, $from);
	
	$res = array("message_ok" => "Demande d'annulation envoyée.");
	return $res;
}

