<?php
/*
 *  Copyright 2020, Florence Henry, LESIA / CNRS - Observatoire de Paris - PSL
 *  florence.henry@obspm.fr
 *  https://gitlab.obspm.fr/henry/outil-de-gestion-des-missions
 *
 *  This file is part of "OGM - Outil de gestion des missions".
 *  
 *  OGM is free software: you can redistribute it and/or modify
 *  it under the terms of the Affero GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  OGM is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OGM.
 */

if (!defined("_ECRIRE_INC_VERSION")) return;


function formulaires_demander_confirmation_mission_saisies($id_reponse, $chef){
	$saisies = array(
		array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'id_reponse',
				'defaut' => $id_reponse,
				'obligatoire' => 'oui'
			)
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'explication' => 'Vous pouvez rectifier l\'adresse email si nécessaire',
				'nom' => 'chef',
				'defaut' => $chef,
			),
			'verifier' => array(
				'type' => 'email',
			),
		),
	);
	
	return $saisies;
}



function formulaires_demander_confirmation_mission_verifier(){
	$erreurs = array();
	$message_erreur = array();
	
	$id_reponse = intval(_request('id_reponse'));
	
	$where = array();
	$where[] = "id_formulaires_reponse = $id_reponse" ;

	$res = sql_countsel("spip_formulaires_reponses", $where);
	if ($res != 1) {
		$message_erreur[] = "Identifiant invalide.";
	}
	
	if (_request('chef') == '') {
		$message_erreur[] = "Vous devez indiquer une adresse email.";
	}
	
	if (count($message_erreur) > 0) {
		$erreurs['message_erreur'] = "Impossible d'envoyer la demande.<br/>" . 
			implode('<br/>', $message_erreur);
	}

	return $erreurs;
}

function formulaires_demander_confirmation_mission_traiter(){
	$id_reponse = intval(_request('id_reponse'));
	$where = array();
	$where[] = "id_formulaires_reponse = $id_reponse" ;
	$where[] = "nom = 'input_4'" ; // nom du missionnaire

	$agent = sql_getfetsel("valeur", "spip_formulaires_reponses_champs", $where);

	$page = recuperer_fond("notifications/demande_confirmation_mission", array('id_reponse' => $id_reponse));

	$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
	
	$destinataire = explode(',', _request('chef'));
	$sujet = "Demande de confirmation de mission pour $agent";
	$corps = array( "html" => $page, "nom_envoyeur" => utf8_decode($GLOBALS['visiteur_session']['nom']) );
	$from = $GLOBALS['visiteur_session']['email'];
	
	$envoyer_mail($destinataire, $sujet, $corps, $from);
	
	$res = array("message_ok" => "Demande de confirmation envoyée.");
	return $res;
}

